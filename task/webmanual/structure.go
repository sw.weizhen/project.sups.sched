package webmanual

import (
	syn "gitlab.com/sw.weizhen/project.sups.sched/sync.relay"
)

type SyncRelayInfo struct {
	Size int                     `json:"size"`
	CMAP map[string]syn.Metadata `json:"task"`
}
