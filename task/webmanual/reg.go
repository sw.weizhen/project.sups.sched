package webmanual

import (
	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
)

func Reg() {

	opv.MUX.All("/sched/ping", apiPing)
	opv.MUX.Get("/sched/ver", apiVer)
	opv.MUX.Get("/sched/AllowIP", apiAllowIP)
	opv.MUX.Get("/sched/sync/info", apiSyncRelayInfo)

	// opv.MUX.Get("/sched/mamual/phr/agentgame/{db}/{commence}/{cease}", apiMAM)
	// opv.MUX.Get("/sched/mamual/asm/{task}/{domain}/{commence}/{cease}", apiMAM)

	opv.MUX.Get("/sched/manual/phr/{db}/{head}/{tail}", apiManualPHR)
	opv.MUX.Get("/sched/manual/subagents/{db}/{task}/{head}/{tail}", apiManualSubagents)
}
