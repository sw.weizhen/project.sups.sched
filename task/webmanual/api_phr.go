package webmanual

import (
	"fmt"
	"strings"
	"time"

	cf "gitlab.com/sw.weizhen/project.sups.sched/module/conf"
	ct "gitlab.com/sw.weizhen/util.calc.timer"

	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
	syn "gitlab.com/sw.weizhen/project.sups.sched/sync.relay"
	sq "gitlab.com/sw.weizhen/project.sups.sched/task/phr/sqlscript"
	db "gitlab.com/sw.weizhen/rdbms.mysql"
)

func manPHR(head, tail, ip string, dbsrcCfg cf.DBSrcCfg) {

	t1 := time.Now().Unix()

	interrupt := false

	metedata := syn.Metadata{
		Tag:    "manual",
		Domain: dbsrcCfg.Domain,
		DB:     dbsrcCfg.Host,
		Type:   syn.CST_MD_TYPE_MANUAL,
		MPKey:  "manPHR",

		ForcePass: false,
		Priority:  syn.CST_MD_PRIORITY_LOW,
		Timestamp: time.Now().Unix(),
	}

	metedata.SetIP(ip)

	dbdes := opv.DBDes
	dbdesPtr, err := db.New(dbdes.User, dbdes.Password, dbdes.Host, dbdes.Port, opv.CST_DB_Superset, dbdes.Charset, 1, 1)
	if err != nil {
		opv.L.Error("%s(%s)::MAN(PHR)::agentgame::dbdesPtr, err -> %v", opv.CST_DB_Superset, dbdes.Hostname, err)
		return
	}

	dbsrcPtr, err := db.New(dbsrcCfg.User, dbsrcCfg.Password, dbsrcCfg.Host, dbsrcCfg.Port, opv.CST_DB_Default, dbsrcCfg.Charset, 1, 1)
	if err != nil {
		opv.L.Error("%s(%s)::MAN(PHR)::agentgame::dbsrcPtr, err -> %v", dbsrcCfg.Domain, dbsrcCfg.Hostname, err)
		return
	}

	curDay := ""

	defer func() {
		dbdesPtr.Close()
		dbsrcPtr.Close()

		if interrupt {
			syn.CHLMAMDone <- metedata
		}

		syn.MPManual.Del(metedata.MPKey)

		cost := opv.FMTSecToHour(time.Now().Unix() - t1)

		opv.L.Info("%s(%s)::MAN(PHR)::agentgame::done, interrupt: %s, last date: %s, container size: %d, cost: %s", dbsrcCfg.Domain, dbsrcCfg.Hostname, interrupt, curDay, syn.MPManual.Len(), cost)

	}()

	qGMRs, err := dbsrcPtr.Query(sq.QueryAllGMRecord(dbsrcCfg.PerHourEXCLSchema))
	if err != nil {
		opv.L.Error("%s(%s)::MAN(PHR)::agentgame::QueryAllGMRecord, err -> %v", dbsrcCfg.Domain, dbsrcCfg.Hostname, err)
		return
	}

	if qGMRs.Length == 0 {
		opv.L.Warning("%s(%s)::MAN(PHR)::agentgame::QueryAllGMRecord -> no found game data", dbsrcCfg.Domain, dbsrcCfg.Hostname)
		return
	}

	days, err := calcDays(head, tail)
	if err != nil {
		opv.L.Error("%s(%s)::MAN(PHR)::agentgame::calcDays, err -> %v", dbsrcCfg.Domain, dbsrcCfg.Hostname, err)
		return
	}

	syn.MPManual.Put(metedata.MPKey, metedata)

LBLMAN_PHR:
	for _, day := range days {
		select {
		case <-syn.CHLMAMDeny:
			interrupt = true
			break LBLMAN_PHR
		default:
			curDay = day
			if err := phrPerGMRs(day, qGMRs.RowsResponse, dbsrcCfg, dbsrcPtr, dbdesPtr); err != nil {
				break LBLMAN_PHR
			}
		}
	}

}

func phrPerGMRs(day string, qGMRs []map[string]string, dbsrcCfg cf.DBSrcCfg, dbsrcPtr, dbdesPtr *db.DBOperator) error {

	cmpDay := strings.ReplaceAll(day, "-", "")

	for _, gmr := range qGMRs {
		schema := gmr["TABLE_SCHEMA"]
		gmRecordOnMonthRes, err := dbsrcPtr.Query(sq.QueryTBLsGameRecord(schema, cmpDay))
		if err != nil {
			return err
		}

		if gmRecordOnMonthRes.Length > 0 {
			if err := phr(day, schema, gmRecordOnMonthRes.RowsResponse[0]["TBL"], dbsrcCfg, dbsrcPtr, dbdesPtr); err != nil {
				return err
			}
		}
	}

	return nil
}

func phr(date, schema, tbl string, dbsrcCfg cf.DBSrcCfg, dbsrcPtr, dbdesPtr *db.DBOperator) error {
	yyyymm := strings.ReplaceAll(date, "-", "")[:6]
	tblName := fmt.Sprintf(opv.CST_TBL_PerHourAgentGame_yyyymm, yyyymm)

	resTBL, err := dbdesPtr.Query(sq.QueryTBL(opv.CST_DB_Superset, tblName))
	if err != nil {
		return err
	}

	if resTBL.Length == 0 {
		return fmt.Errorf("no found table %s", tblName)
	}

	timeline, err := calcDayTimeline(date)
	if err != nil {
		return err
	}

	for i := 0; i < len(timeline); i++ {
		head := timeline[i]
		tail := ct.New(ct.OptInput(head)).Add(1 * 60 * 60).Layout(ct.CST_FMT_YMDHMS)

		res, err := dbsrcPtr.Query(sq.QueryCalcGMRecordPerHour(schema, tbl, head, tail))
		if err != nil {
			return err
		}

		if res.Length > 0 {
			_, err = dbdesPtr.Exec(sq.ReplacePerHourAgentGame(yyyymm, schema, tbl, dbsrcCfg.Domain, dbsrcCfg.Hostname, res))
			if err != nil {
				return err
			}
		}
	}

	return nil
}
