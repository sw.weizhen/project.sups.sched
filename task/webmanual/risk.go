package webmanual

import (
	"fmt"
	"net/http"
	"strings"

	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
	core "gitlab.com/sw.weizhen/project.sups.sched/module/http.restful"
	sopv "gitlab.com/sw.weizhen/project.sups.sched/sync.relay"

	ct "gitlab.com/sw.weizhen/util.calc.timer"
)

func risk(req *http.Request) (int, string) {

	if sopv.MPSched.Len() > 0 {
		return 10000, "deny: Sched is running"
	}

	if sopv.MPManual.Len() > 0 {
		return 10005, "deny: MAM is running"
	}

	remoteAddr := req.RemoteAddr
	ip := remoteAddr[:strings.LastIndex(remoteAddr, ":")]
	if !opv.AllowIP[ip] {
		return 10010, "deny: ip address not allowed"
	}

	param := core.URLReqParamMap(req)

	commence := param["head"]
	t1 := ct.New(
		ct.OptInput(commence),
		ct.OptFMTinput(ct.CST_FMT_YMD_CMP),
	)

	// commence.Layout(ct.CST_FMT_YMDHMS)
	if !t1.ChkTime() {
		return 10015, fmt.Sprintf("invalid head date format: %s -> %s", commence, t1.String())
	}

	cease := param["tail"]
	t2 := ct.New(
		ct.OptInput(cease),
		ct.OptFMTinput(ct.CST_FMT_YMD_CMP),
	)

	// cease.Layout(ct.CST_FMT_YMDHMS)
	if !t2.ChkTime() {
		return 10016, fmt.Sprintf("invalid tail date format: %s -> %s", cease, t2.String())
	}

	cmp := t1.Compare(t2)
	if cmp > 0 {
		return 10017, fmt.Sprintf("head: %s tail: %s, head date must less than or equal to tail date", commence, cease)
	}

	yesterday := ct.New().Add(-24 * 60 * 60)
	yesterday.Layout(ct.CST_FMT_YMD)
	cmp = t2.Compare(yesterday)
	if cmp >= 0 {
		return 10018, fmt.Sprintf("limit: %s tail: %s, exceed the time limit", yesterday, cease)
	}

	db := param["db"]
	cfgDB := opv.CFG.RDBCfg.Source
	ok := false
	for _, srcDB := range cfgDB {
		if srcDB.Hostname == db {
			ok = true
			break
		}
	}

	if !ok {
		return 10025, fmt.Sprintf("no found db %s", db)
	}

	return -1, ""
}
