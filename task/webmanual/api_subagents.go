package webmanual

import (
	"errors"
	"time"

	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
	cf "gitlab.com/sw.weizhen/project.sups.sched/module/conf"
	syn "gitlab.com/sw.weizhen/project.sups.sched/sync.relay"
	db "gitlab.com/sw.weizhen/rdbms.mysql"

	subagents "gitlab.com/sw.weizhen/project.sups.sched/task/subagents"
)

func prepareAgentSet(dbsrcCfg cf.DBSrcCfg, dbsrcPtr *db.DBOperator) ([]string, error) {
	roots, err := subagents.PrepareAgentRoots(dbsrcCfg, dbsrcPtr)
	if err != nil {
		return nil, err
	}

	agentSet := subagents.RootMapToSliceStr(roots)
	if len(agentSet) == 0 {
		return nil, errors.New("empty subagent container")
	}

	return agentSet, nil
}

func manSubagentsStatisusers(ip string, days []string, dbsrcCfg cf.DBSrcCfg) {
	t1 := time.Now().Unix()

	interrupt := false

	metedata := syn.Metadata{
		Tag:    "manual",
		Domain: dbsrcCfg.Domain,
		DB:     dbsrcCfg.Host,
		Type:   syn.CST_MD_TYPE_MANUAL,
		MPKey:  "manSubagentsStatisusers",

		ForcePass: false,
		Priority:  syn.CST_MD_PRIORITY_LOW,
		Timestamp: time.Now().Unix(),
	}

	metedata.SetIP(ip)

	dbdes := opv.DBDes
	dbdesPtr, err := db.New(dbdes.User, dbdes.Password, dbdes.Host, dbdes.Port, opv.CST_DB_Superset, dbdes.Charset, 1, 1)
	if err != nil {
		opv.L.Error("%s(%s)::MAN(Subagents)::statisusers::dbdesPtr, err -> %v", opv.CST_DB_Superset, dbdes.Hostname, err)
		return
	}

	dbsrcPtr, err := db.New(dbsrcCfg.User, dbsrcCfg.Password, dbsrcCfg.Host, dbsrcCfg.Port, opv.CST_DB_Default, dbsrcCfg.Charset, 1, 1)
	if err != nil {
		opv.L.Error("%s(%s)::MAN(Subagents)::statisusers::dbsrcPtr, err -> %v", dbsrcCfg.Domain, dbsrcCfg.Hostname, err)
		return
	}

	curDay := ""

	defer func() {
		dbdesPtr.Close()
		dbsrcPtr.Close()

		if interrupt {
			syn.CHLMAMDone <- metedata
		}

		syn.MPManual.Del(metedata.MPKey)

		cost := opv.FMTSecToHour(time.Now().Unix() - t1)

		opv.L.Info("%s(%s)::MAN(Subagents)::statisusers::done, interrupt: %t, last date: %s, container size: %d, cost: %s", dbsrcCfg.Domain, dbsrcCfg.Hostname, interrupt, curDay, syn.MPManual.Len(), cost)
	}()

	agentSet, err := prepareAgentSet(dbsrcCfg, dbsrcPtr)
	if err != nil {
		opv.L.Error("%s(%s)::MAN(Subagents)::statisusers::prepareAgentSet, err -> %v", dbsrcCfg.Domain, dbsrcCfg.Hostname, err)
		return
	}

	syn.MPManual.Put(metedata.MPKey, metedata)

LBLMAN_SUBAGENTS:
	for _, date := range days {
		select {
		case <-syn.CHLMAMDeny:
			interrupt = true
			break LBLMAN_SUBAGENTS

		default:
			curDay = date
			subagents.ComputeStatisusersBrandPerDay(date, agentSet, dbsrcCfg, dbdesPtr)
		}
	}
}

func manSubagentsReguser(ip string, days []string, dbsrcCfg cf.DBSrcCfg) {
	t1 := time.Now().Unix()

	interrupt := false

	metedata := syn.Metadata{
		Tag:    "manual",
		Domain: dbsrcCfg.Domain,
		DB:     dbsrcCfg.Host,
		Type:   syn.CST_MD_TYPE_MANUAL,
		MPKey:  "manSubagentsReguser",

		ForcePass: false,
		Priority:  syn.CST_MD_PRIORITY_LOW,
		Timestamp: time.Now().Unix(),
	}

	metedata.SetIP(ip)

	dbdes := opv.DBDes
	dbdesPtr, err := db.New(dbdes.User, dbdes.Password, dbdes.Host, dbdes.Port, opv.CST_DB_Superset, dbdes.Charset, 1, 1)
	if err != nil {
		opv.L.Error("%s(%s)::MAN(Subagents)::Reguser::dbdesPtr, err -> %v", opv.CST_DB_Superset, dbdes.Hostname, err)
		return
	}

	dbsrcPtr, err := db.New(dbsrcCfg.User, dbsrcCfg.Password, dbsrcCfg.Host, dbsrcCfg.Port, opv.CST_DB_Default, dbsrcCfg.Charset, 1, 1)
	if err != nil {
		opv.L.Error("%s(%s)::MAN(Subagents)::Reguser::dbsrcPtr, err -> %v", dbsrcCfg.Domain, dbsrcCfg.Hostname, err)
		return
	}

	curDay := ""

	defer func() {
		dbdesPtr.Close()
		dbsrcPtr.Close()

		if interrupt {
			syn.CHLMAMDone <- metedata
		}

		syn.MPManual.Del(metedata.MPKey)

		cost := opv.FMTSecToHour(time.Now().Unix() - t1)

		opv.L.Info("%s(%s)::MAN(Subagents)::Reguser::done, interrupt: %t, last date: %s, container size: %d, cost: %s", dbsrcCfg.Domain, dbsrcCfg.Hostname, interrupt, curDay, syn.MPManual.Len(), cost)
	}()

	agentSet, err := prepareAgentSet(dbsrcCfg, dbsrcPtr)
	if err != nil {
		opv.L.Error("%s(%s)::MAN(Subagents)::Reguser::prepareAgentSet, err -> %v", dbsrcCfg.Domain, dbsrcCfg.Hostname, err)
		return
	}

	syn.MPManual.Put(metedata.MPKey, metedata)

LBLMAN_SUBAGENTS:
	for _, date := range days {
		select {
		case <-syn.CHLMAMDeny:
			interrupt = true
			break LBLMAN_SUBAGENTS

		default:
			curDay = date
			subagents.ComputeAgentReguserBrandPerDay(date, agentSet, dbsrcCfg, dbdesPtr)
		}
	}

}

func manSubagentsAgentAll(ip string, days []string, dbsrcCfg cf.DBSrcCfg) {
	t1 := time.Now().Unix()

	interrupt := false

	metedata := syn.Metadata{
		Tag:    "manual",
		Domain: dbsrcCfg.Domain,
		DB:     dbsrcCfg.Host,
		Type:   syn.CST_MD_TYPE_MANUAL,
		MPKey:  "manSubagentsAgentAll",

		ForcePass: false,
		Priority:  syn.CST_MD_PRIORITY_LOW,
		Timestamp: time.Now().Unix(),
	}

	metedata.SetIP(ip)

	dbdes := opv.DBDes
	dbdesPtr, err := db.New(dbdes.User, dbdes.Password, dbdes.Host, dbdes.Port, opv.CST_DB_Superset, dbdes.Charset, 1, 1)
	if err != nil {
		opv.L.Error("%s(%s)::MAN(Subagents)::agentall::dbdesPtr, err -> %v", opv.CST_DB_Superset, dbdes.Hostname, err)
		return
	}

	dbsrcPtr, err := db.New(dbsrcCfg.User, dbsrcCfg.Password, dbsrcCfg.Host, dbsrcCfg.Port, opv.CST_DB_Default, dbsrcCfg.Charset, 1, 1)
	if err != nil {
		opv.L.Error("%s(%s)::MAN(Subagents)::agentall::dbsrcPtr, err -> %v", dbsrcCfg.Domain, dbsrcCfg.Hostname, err)
		return
	}

	curDay := ""

	defer func() {
		dbdesPtr.Close()
		dbsrcPtr.Close()

		if interrupt {
			syn.CHLMAMDone <- metedata
		}

		syn.MPManual.Del(metedata.MPKey)

		cost := opv.FMTSecToHour(time.Now().Unix() - t1)

		opv.L.Info("%s(%s)::MAN(Subagents)::agentall::done, interrupt: %t, last date: %s, container size: %d, cost: %s", dbsrcCfg.Domain, dbsrcCfg.Hostname, interrupt, curDay, syn.MPManual.Len(), cost)
	}()

	agentSet, err := prepareAgentSet(dbsrcCfg, dbsrcPtr)
	if err != nil {
		opv.L.Error("%s(%s)::MAN(Subagents)::agentall::prepareAgentSet, err -> %v", dbsrcCfg.Domain, dbsrcCfg.Hostname, err)
		return
	}

	syn.MPManual.Put(metedata.MPKey, metedata)

LBLMAN_SUBAGENTS:
	for _, date := range days {
		select {
		case <-syn.CHLMAMDeny:
			interrupt = true
			break LBLMAN_SUBAGENTS

		default:
			curDay = date
			subagents.ComputeAgentAllBrandPerDay(date, agentSet, dbsrcCfg, dbdesPtr)
		}
	}
}

func manSubagentsGM(ip string, days []string, dbsrcCfg cf.DBSrcCfg) {

	t1 := time.Now().Unix()

	interrupt := false

	metedata := syn.Metadata{
		Tag:    "manual",
		Domain: dbsrcCfg.Domain,
		DB:     dbsrcCfg.Host,
		Type:   syn.CST_MD_TYPE_MANUAL,
		MPKey:  "manSubagentsAgentGame",

		ForcePass: false,
		Priority:  syn.CST_MD_PRIORITY_LOW,
		Timestamp: time.Now().Unix(),
	}

	metedata.SetIP(ip)

	dbdes := opv.DBDes
	dbdesPtr, err := db.New(dbdes.User, dbdes.Password, dbdes.Host, dbdes.Port, opv.CST_DB_Superset, dbdes.Charset, 1, 1)
	if err != nil {
		opv.L.Error("%s(%s)::MAN(Subagents)::agentgame::dbdesPtr, err -> %v", opv.CST_DB_Superset, dbdes.Hostname, err)
		return
	}

	dbsrcPtr, err := db.New(dbsrcCfg.User, dbsrcCfg.Password, dbsrcCfg.Host, dbsrcCfg.Port, opv.CST_DB_Default, dbsrcCfg.Charset, 1, 1)
	if err != nil {
		opv.L.Error("%s(%s)::MAN(Subagents)::agentgame::dbsrcPtr, err -> %v", dbsrcCfg.Domain, dbsrcCfg.Hostname, err)
		return
	}

	curDay := ""

	defer func() {
		dbdesPtr.Close()
		dbsrcPtr.Close()

		if interrupt {
			syn.CHLMAMDone <- metedata
		}

		syn.MPManual.Del(metedata.MPKey)

		cost := opv.FMTSecToHour(time.Now().Unix() - t1)

		opv.L.Info("%s(%s)::MAN(Subagents)::agentgame::done, interrupt: %t, last date: %s, container size: %d, cost: %s", dbsrcCfg.Domain, dbsrcCfg.Hostname, interrupt, curDay, syn.MPManual.Len(), cost)
	}()

	agentSet, err := prepareAgentSet(dbsrcCfg, dbsrcPtr)
	if err != nil {
		opv.L.Error("%s(%s)::MAN(Subagents)::agentgame::prepareAgentSet, err -> %v", dbsrcCfg.Domain, dbsrcCfg.Hostname, err)
		return
	}

	syn.MPManual.Put(metedata.MPKey, metedata)

LBLMAN_SUBAGENTS:
	for _, date := range days {
		select {
		case <-syn.CHLMAMDeny:
			interrupt = true
			break LBLMAN_SUBAGENTS

		default:
			curDay = date
			subagents.ComputeAgentGMBrandPerDay(date, agentSet, dbsrcCfg, dbdesPtr)
		}
	}

}
