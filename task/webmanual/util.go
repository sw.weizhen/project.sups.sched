package webmanual

import (
	ct "gitlab.com/sw.weizhen/util.calc.timer"
)

func calcDays(head, tail string) ([]string, error) {
	days, err := ct.New(
		ct.OptInput(head),
		ct.OptFMTinput(ct.CST_FMT_YMD_CMP),
	).CALCTimeline(
		ct.OptLimitTime(ct.New(
			ct.OptInput(tail),
			ct.OptFMTinput(ct.CST_FMT_YMD_CMP),
		).Add(24*60*60)),
		ct.OptLimitTimeOverlap(false),
		ct.OptOutFMT(ct.CST_FMT_YMD),
		ct.OptINVLMode(ct.ENUM_INVL_DAY),
	)

	if err != nil {
		return nil, err
	}

	return days, err
}

func calcDayTimeline(date string) ([]string, error) {
	timeline, err := ct.New(
		ct.OptInput(date),
		ct.OptFMTinput(ct.CST_FMT_YMD),
	).CALCTimeline(
		ct.OptLimitTime(ct.New(
			ct.OptInput(date),
			ct.OptFMTinput(ct.CST_FMT_YMD),
		).Add(24*60*60)),
		ct.OptLimitTimeOverlap(false),
		ct.OptOutFMT(ct.CST_FMT_YMDHMS),
	)

	if err != nil {
		return nil, err
	}

	return timeline, nil
}
