package webmanual

import (
	"net/http"
	"strings"

	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
	sresp "gitlab.com/sw.weizhen/project.sups.sched/module/http.resp"
	syn "gitlab.com/sw.weizhen/project.sups.sched/sync.relay"

	cf "gitlab.com/sw.weizhen/project.sups.sched/module/conf"
	core "gitlab.com/sw.weizhen/project.sups.sched/module/http.restful"
	ct "gitlab.com/sw.weizhen/util.calc.timer"
)

// sched/ping
func apiPing(resp http.ResponseWriter, req *http.Request) {
	resp.Write(sresp.CodeResponseSuccess("pong"))
}

// sched/version
func apiVer(resp http.ResponseWriter, req *http.Request) {
	resp.Write(sresp.CodeResponseSuccess(VERSION))
}

// sched/AllowIP
func apiAllowIP(resp http.ResponseWriter, req *http.Request) {
	resp.Write(sresp.CodeResponseSuccess(opv.AllowIP))
}

// sched/sync/info
func apiSyncRelayInfo(resp http.ResponseWriter, req *http.Request) {

	sched := SyncRelayInfo{
		Size: syn.MPSched.Len(),
		CMAP: syn.MPSched.Data(),
	}

	mam := SyncRelayInfo{
		Size: syn.MPManual.Len(),
		CMAP: syn.MPManual.Data(),
	}

	mp := map[string]interface{}{
		"MAM":             mam,
		"SCHED":           sched,
		"CHLSchedTrigger": len(syn.CHLSchedTrigger),
		"CHLSchedPass":    len(syn.CHLSchedPass),
		"CHLMAMTrigger":   len(syn.CHLMAMTrigger),
		"CHLMAMDeny":      len(syn.CHLMAMDeny),
		"CHLMAMDone":      len(syn.CHLMAMDone),
	}

	resp.Write(sresp.CodeResponseSuccess(mp))
}

func apiManualPHR(resp http.ResponseWriter, req *http.Request) {

	if errCode, errMsg := risk(req); errCode != -1 {
		resp.Write(sresp.CodeResponseAbstraction(false, errCode, errMsg, nil, false))
		return
	}

	param := core.URLReqParamMap(req)
	head := param["head"]
	tail := param["tail"]
	db := param["db"]

	var rdb cf.DBSrcCfg
	for _, srcDB := range opv.CFG.RDBCfg.Source {
		if srcDB.Hostname == db {
			rdb = srcDB
		}
	}

	remoteAddr := req.RemoteAddr
	ip := remoteAddr[:strings.LastIndex(remoteAddr, ":")]

	go manPHR(head, tail, ip, rdb)

	resp.Write(sresp.CodeResponseSuccess(nil))
}

func apiManualSubagents(resp http.ResponseWriter, req *http.Request) {

	if errCode, errMsg := risk(req); errCode != -1 {
		resp.Write(sresp.CodeResponseAbstraction(false, errCode, errMsg, nil, false))
		return
	}

	param := core.URLReqParamMap(req)
	head := param["head"]
	tail := param["tail"]
	db := param["db"]
	task := param["task"]

	var rdb cf.DBSrcCfg
	for _, srcDB := range opv.CFG.RDBCfg.Source {
		if srcDB.Hostname == db {
			rdb = srcDB
		}
	}

	remoteAddr := req.RemoteAddr
	ip := remoteAddr[:strings.LastIndex(remoteAddr, ":")]

	days, err := ct.New(
		ct.OptInput(head),
		ct.OptFMTinput(ct.CST_FMT_YMD_CMP),
	).CALCTimeline(
		ct.OptLimitTime(
			ct.New(
				ct.OptInput(tail),
				ct.OptFMTinput(ct.CST_FMT_YMD_CMP),
			),
		),
		ct.OptINVLMode(ct.ENUM_INVL_DAY),
		ct.OptOutFMT(ct.CST_FMT_YMD),
	)

	if err != nil {
		resp.Write(sresp.CodeResponseAbstraction(false, 20000, err.Error(), nil, false))
		return
	}

	switch task {
	case syn.CST_MD_TASK_AGENTGAME:
		go manSubagentsGM(ip, days, rdb)

	case syn.CST_MD_TASK_AGENTALL:
		go manSubagentsAgentAll(ip, days, rdb)

	case syn.CST_MD_TASK_REGUSER:
		go manSubagentsReguser(ip, days, rdb)

	case syn.CST_MD_TASK_STATISUSERS:
		go manSubagentsStatisusers(ip, days, rdb)

	default:
		resp.Write(sresp.CodeResponseAbstraction(false, 20000, "unknow task", nil, false))
		return
	}

	resp.Write(sresp.CodeResponseSuccess(nil))
}
