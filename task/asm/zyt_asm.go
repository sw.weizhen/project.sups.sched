package asm

import (
	"sync"

	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
	cf "gitlab.com/sw.weizhen/project.sups.sched/module/conf"
)

func Recall() {

	if err := prepareTBL(); err != nil {
		opv.L.Error("asm::prepareTBL failed, err -> %v", err)
		return
	}

	var wgASM sync.WaitGroup
	for i := 0; i < len(opv.CFG.RDBCfg.Source); i++ {
		asmCfgdb := opv.CFG.RDBCfg.Source[i]

		head := asmCfgdb.Head.ASM

		if asmCfgdb.Enable && len(head) > 0 {
			wgASM.Add(1)
			go func(dbsrcCfg cf.DBSrcCfg) {
				if err := recall(false, head, opv.CFG.CaptureCfg.Batch, dbsrcCfg); err != nil {
					opv.L.Error("ASM::Recall(%s, %s:%d), err -> %v", asmCfgdb.Hostname, asmCfgdb.Host, asmCfgdb.Port, err)
				}

				wgASM.Done()

			}(asmCfgdb)
		}
	}

	wgASM.Wait()

	opv.L.Info("ASM DONE")
}
