package asm

import (
	"fmt"
	"time"

	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
	cf "gitlab.com/sw.weizhen/project.sups.sched/module/conf"
	sq "gitlab.com/sw.weizhen/project.sups.sched/task/asm/sqlscript"
	db "gitlab.com/sw.weizhen/rdbms.mysql"
)

func replaceSupersetStatisRecordAgentAllReguser(sourceDB, sourceDBSub string, selResponse *db.DBResponse, dbdesPtr *db.DBOperator) error {
	sqlSyntax := sq.ReplaceReguser(sourceDB, sourceDBSub, selResponse)
	_, err := dbdesPtr.Exec(sqlSyntax)

	return err
}

func batchStatisRecordAgentAllReguser(head string, batch int, dbsrcCfg cf.DBSrcCfg, dbdesPtr, dbsrcPtr *db.DBOperator) {

	i := 0
	for {
		resp, err := dbsrcPtr.Query(sq.QueryReguserRNG(head, fmt.Sprintf("%d", i), fmt.Sprintf("%d", batch)))
		if err != nil {
			opv.L.Error("%s(%s)::batch::QueryReguserRNG, err -> %v", dbsrcCfg.Domain, dbsrcCfg.Hostname, err)
			break
		}

		if resp.Length == 0 {
			break
		}

		err = replaceSupersetStatisRecordAgentAllReguser(dbsrcCfg.Domain, dbsrcCfg.Hostname, resp, dbdesPtr)
		if err != nil {
			opv.L.Error("%s(%s)::batch::replaceSupersetStatisRecordAgentAllReguser, err -> %v", dbsrcCfg.Domain, dbsrcCfg.Hostname, err)
			break
		}

		if resp.Length < uint32(batch) {
			break
		}

		i = i + batch
	}
}

func asmStatisRecordAgentAllReguser(fromLastDate bool, head string, batch int, dbAsmCfg cf.DBSrcCfg, dbdesptr, dbasmptr *db.DBOperator) error {

	ctHead := head

	if fromLastDate {
		respLastData, err := dbdesptr.Query(sq.QueryReguserLastDate(dbAsmCfg.Domain, dbAsmCfg.Hostname))
		if err != nil {
			return err
		}

		if respLastData.Length != 0 {
			ctHead = respLastData.RowsResponse[0]["StatisDate"]
		}
	}

	t1 := time.Now().Unix()

	batchStatisRecordAgentAllReguser(ctHead, batch, dbAsmCfg, dbdesptr, dbasmptr)

	cost := opv.FMTSecToHour(time.Now().Unix() - t1)

	opv.L.Info("asm done => reguser: %s(%s, %s) -> cost: %s", dbAsmCfg.Domain, dbAsmCfg.Hostname, ctHead, cost)

	return nil
}
