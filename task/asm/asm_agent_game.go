package asm

import (
	"fmt"
	"time"

	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
	cf "gitlab.com/sw.weizhen/project.sups.sched/module/conf"
	sq "gitlab.com/sw.weizhen/project.sups.sched/task/asm/sqlscript"
	db "gitlab.com/sw.weizhen/rdbms.mysql"
)

func replaceSupersetStatisRecordAgentGame(sourceDB, sourceDBSub string, selResponse *db.DBResponse, dbdesptr *db.DBOperator) error {
	sqlSyntax := sq.ReplaceAgentGame(sourceDB, sourceDBSub, selResponse)
	_, err := dbdesptr.Exec(sqlSyntax)

	return err
}

func batchStatisRecordAgentGame(head string, batch int, dbsrcCfg cf.DBSrcCfg, dbdesPtr, dbsrcPtr *db.DBOperator) {

	i := 0
	for {
		respStatisusers, err := dbsrcPtr.Query(sq.QueryAgentGameRNG(head, fmt.Sprintf("%d", i), fmt.Sprintf("%d", batch)))
		if err != nil {
			opv.L.Error("%s(%s)::batch::QueryAgentGameRNG, err -> %v", dbsrcCfg.Domain, dbsrcCfg.Hostname, err)
			break
		}

		if respStatisusers.Length == 0 {
			break
		}

		err = replaceSupersetStatisRecordAgentGame(dbsrcCfg.Domain, dbsrcCfg.Hostname, respStatisusers, dbdesPtr)
		if err != nil {
			opv.L.Error("%s(%s)::batch::replaceSupersetStatisRecordAgentGame, err -> %v", dbsrcCfg.Domain, dbsrcCfg.Hostname, err)
			break
		}

		if respStatisusers.Length < uint32(batch) {
			break
		}

		i = i + batch
	}
}

func asmStatisRecordAgentGame(fromLastDate bool, head string, batch int, dbsrcCfg cf.DBSrcCfg, dbdesPtr, dbsrcPtr *db.DBOperator) error {

	ctHead := head

	if fromLastDate {
		respLastData, err := dbdesPtr.Query(sq.QuerySupersetAgentGameLast(dbsrcCfg.Domain, dbsrcCfg.Hostname))
		if err != nil {
			return err
		}

		if respLastData.Length != 0 {
			ctHead = respLastData.RowsResponse[0]["StatisDate"]
		}
	}

	t1 := time.Now().Unix()

	batchStatisRecordAgentGame(ctHead, batch, dbsrcCfg, dbdesPtr, dbsrcPtr)

	cost := opv.FMTSecToHour(time.Now().Unix() - t1)

	opv.L.Info("asm done => agent_game: %s(%s, %s) -> cost: %s", dbsrcCfg.Domain, dbsrcCfg.Hostname, ctHead, cost)

	return nil
}
