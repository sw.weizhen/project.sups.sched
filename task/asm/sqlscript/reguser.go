package sqlscript

import (
	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
	swmysql "gitlab.com/sw.weizhen/rdbms.mysql"

	"fmt"
	"strings"
)

func CreateTBLReguserDomain(domain string) string {
	var sb strings.Builder

	tblName := fmt.Sprintf(opv.CST_TBL_ASMReguser, domain)

	sb.WriteString("CREATE TABLE IF NOT EXISTS `" + tblName + "` (")
	sb.WriteString(" `StatisDate` date NOT NULL,")
	sb.WriteString(" `ChannelID` int(11) NOT NULL,")
	sb.WriteString(" `WinGold` bigint(20) DEFAULT NULL,")
	sb.WriteString(" `LostGold` bigint(20) DEFAULT NULL,")
	sb.WriteString(" `CellScore` bigint(20) DEFAULT NULL,")
	sb.WriteString(" `Revenue` bigint(20) DEFAULT NULL,")
	sb.WriteString(" `WinNum` int(11) DEFAULT NULL,")
	sb.WriteString(" `LostNum` int(11) DEFAULT NULL,")
	sb.WriteString(" `ActiveUsers` int(11) DEFAULT NULL,")
	sb.WriteString(" `SourceDB` varchar(20) NOT NULL,")
	sb.WriteString(" `SourceDBSub` varchar(20) NOT NULL,")
	sb.WriteString(" `MoveDate` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,")
	sb.WriteString("  PRIMARY KEY (`StatisDate`,`ChannelID`,`SourceDB`,`SourceDBSub`)")
	sb.WriteString(") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;")

	return sb.String()
}

func ReplaceReguser(sourceDB, sourceDBSub string, selResponse *swmysql.DBResponse) string {
	var sb strings.Builder

	tblName := fmt.Sprintf(opv.CST_TBL_ASMReguser, sourceDB)

	sb.WriteString("REPLACE INTO superset." + tblName + " (")
	sb.WriteString(" StatisDate,")
	sb.WriteString(" ChannelID,")
	sb.WriteString(" WinGold,")
	sb.WriteString(" LostGold,")
	sb.WriteString(" CellScore,")
	sb.WriteString(" Revenue,")
	sb.WriteString(" WinNum,")
	sb.WriteString(" LostNum,")
	sb.WriteString(" ActiveUsers,")
	sb.WriteString(" SourceDB,")
	sb.WriteString(" SourceDBSub")
	sb.WriteString(")")
	sb.WriteString("VALUES")

	for i := 0; i < int(selResponse.Length); i++ {

		tmpChannelID := selResponse.RowsResponse[i]["ChannelID"]
		tmpWinGold := selResponse.RowsResponse[i]["WinGold"]
		tmpLostGold := selResponse.RowsResponse[i]["LostGold"]
		tmpCellScore := selResponse.RowsResponse[i]["CellScore"]
		tmpRevenue := selResponse.RowsResponse[i]["Revenue"]
		tmpWinNum := selResponse.RowsResponse[i]["WinNum"]
		tmpLostNum := selResponse.RowsResponse[i]["LostNum"]
		tmpActiveUsers := selResponse.RowsResponse[i]["ActiveUsers"]

		syntax := fmt.Sprintf("('%v', %v, %v, %v, %v, %v, %v, %v, %v, '%v', '%v'),",
			selResponse.RowsResponse[i]["StatisDate"],
			opv.CHKNumVal(tmpChannelID),
			opv.CHKNumVal(tmpWinGold),
			opv.CHKNumVal(tmpLostGold),
			opv.CHKNumVal(tmpCellScore),
			opv.CHKNumVal(tmpRevenue),
			opv.CHKNumVal(tmpWinNum),
			opv.CHKNumVal(tmpLostNum),
			opv.CHKNumVal(tmpActiveUsers),
			sourceDB,
			sourceDBSub,
		)

		sb.WriteString(syntax)
	}

	return sb.String()[:len(sb.String())-1] + ";"
}

func QueryReguserRNG(lastDate, limit, offset string) string {
	var sb strings.Builder

	sb.WriteString("SELECT * FROM KYStatis.statis_record_agent_all_reguser s ")
	sb.WriteString("WHERE s.StatisDate >= '" + lastDate + "' ")
	sb.WriteString("ORDER BY s.StatisDate ASC LIMIT " + limit + ", " + offset + ";")

	return sb.String()
}

func QueryReguserLastDate(sourceDB, sourceDBSub string) string {
	var sb strings.Builder

	tblName := fmt.Sprintf(opv.CST_TBL_ASMReguser, sourceDB)

	sb.WriteString("SELECT sraga.StatisDate AS StatisDate ")
	sb.WriteString("FROM superset." + tblName + " sraga ")
	sb.WriteString("WHERE sraga.SourceDB = '" + sourceDB + "' ")
	sb.WriteString("AND sraga.SourceDBSub = '" + sourceDBSub + "' ")
	sb.WriteString("AND sraga.ChannelID <> -1 ")
	sb.WriteString("ORDER BY sraga.StatisDate DESC ")
	sb.WriteString("LIMIT 1;")

	return sb.String()
}
