package sqlscript

import (
	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
	swmysql "gitlab.com/sw.weizhen/rdbms.mysql"

	"fmt"
	"strings"
)

func CreateTBLStatisusersDomain(domain string) string {
	var sb strings.Builder

	tblName := fmt.Sprintf(opv.CST_TBL_ASMStatisusers, domain)

	sb.WriteString("CREATE TABLE IF NOT EXISTS `" + tblName + "` (")
	sb.WriteString(" `StatisDate` date NOT NULL,")
	sb.WriteString(" `ChannelId` int(11) NOT NULL,")
	sb.WriteString(" `Y_NonLoginUsers` int(11) DEFAULT NULL,")
	sb.WriteString(" `M_NonLoginUsers` int(11) DEFAULT NULL,")
	sb.WriteString(" `H_NonLoginUsers` int(11) DEFAULT NULL,")
	sb.WriteString(" `Y_RegUsers` int(11) DEFAULT NULL,")
	sb.WriteString(" `M_RegUsers` int(11) DEFAULT NULL,")
	sb.WriteString(" `H_RegUsers` int(11) DEFAULT NULL,")
	sb.WriteString(" `Y_PayUsers` int(11) NOT NULL,")
	sb.WriteString(" `M_PayUsers` int(11) DEFAULT NULL,")
	sb.WriteString(" `H_PayUsers` int(11) DEFAULT NULL,")
	sb.WriteString(" `NextRegisterUser` int(11) NOT NULL,")
	sb.WriteString(" `NextLoginUser` int(11) DEFAULT NULL,")
	sb.WriteString(" `ValidNextRegisterUser` int(11) DEFAULT NULL,")
	sb.WriteString(" `ValidNextLoginUser` int(11) DEFAULT NULL,")
	sb.WriteString(" `SevenRegisterUser` int(11) DEFAULT NULL,")
	sb.WriteString(" `SevenLoginUser` int(11) DEFAULT NULL,")
	sb.WriteString(" `ValidSevenRegisterUser` int(11) DEFAULT NULL,")
	sb.WriteString(" `ValidSevenLoginUser` int(11) DEFAULT NULL,")
	sb.WriteString(" `MonthRegisterUser` int(11) DEFAULT NULL,")
	sb.WriteString(" `MonthLoginUser` int(11) DEFAULT NULL,")
	sb.WriteString(" `ValidMonthRegisterUser` int(11) DEFAULT NULL,")
	sb.WriteString(" `ValidMonthLoginUser` int(11) DEFAULT NULL,")
	sb.WriteString(" `DayNewBetUsers` int(11) DEFAULT NULL,")
	sb.WriteString(" `dayNewLogin` int(11) DEFAULT NULL,")
	sb.WriteString(" `SourceDB` varchar(20) NOT NULL,")
	sb.WriteString(" `SourceDBSub` varchar(20) NOT NULL,")
	sb.WriteString(" `MoveDate` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,")
	sb.WriteString(" PRIMARY KEY (`StatisDate`,`ChannelId`, `SourceDB`, `SourceDBSub`)")
	sb.WriteString(") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;")

	return sb.String()
}

func ReplaceStatisusers(sourceDB, sourceDBSub string, selResponse *swmysql.DBResponse) string {
	var sb strings.Builder

	tblName := fmt.Sprintf(opv.CST_TBL_ASMStatisusers, sourceDB)

	sb.WriteString("REPLACE INTO superset." + tblName + " (")
	sb.WriteString(" StatisDate, ")
	sb.WriteString(" ChannelId, ")
	sb.WriteString(" Y_NonLoginUsers, ")
	sb.WriteString(" M_NonLoginUsers, ")
	sb.WriteString(" H_NonLoginUsers, ")
	sb.WriteString(" Y_RegUsers, ")
	sb.WriteString(" M_RegUsers, ")
	sb.WriteString(" H_RegUsers, ")
	sb.WriteString(" Y_PayUsers, ")
	sb.WriteString(" M_PayUsers, ")
	sb.WriteString(" H_PayUsers, ")
	sb.WriteString(" NextRegisterUser, ")
	sb.WriteString(" NextLoginUser, ")
	sb.WriteString(" ValidNextRegisterUser, ")
	sb.WriteString(" ValidNextLoginUser, ")
	sb.WriteString(" SevenRegisterUser, ")
	sb.WriteString(" SevenLoginUser, ")
	sb.WriteString(" ValidSevenRegisterUser, ")
	sb.WriteString(" ValidSevenLoginUser, ")
	sb.WriteString(" MonthRegisterUser, ")
	sb.WriteString(" MonthLoginUser, ")
	sb.WriteString(" ValidMonthRegisterUser, ")
	sb.WriteString(" ValidMonthLoginUser, ")
	sb.WriteString(" DayNewBetUsers, ")
	sb.WriteString(" dayNewLogin, ")
	sb.WriteString(" SourceDB, ")
	sb.WriteString(" SourceDBSub")
	sb.WriteString(")")
	sb.WriteString("VALUES")

	for i := 0; i < int(selResponse.Length); i++ {

		tmpChannelId := selResponse.RowsResponse[i]["ChannelId"]
		tmpY_NonLoginUsers := selResponse.RowsResponse[i]["Y_NonLoginUsers"]
		tmpM_NonLoginUsers := selResponse.RowsResponse[i]["M_NonLoginUsers"]
		tmpH_NonLoginUsers := selResponse.RowsResponse[i]["H_NonLoginUsers"]
		tmpY_RegUsers := selResponse.RowsResponse[i]["Y_RegUsers"]
		tmpM_RegUsers := selResponse.RowsResponse[i]["M_RegUsers"]
		tmpH_RegUsers := selResponse.RowsResponse[i]["H_RegUsers"]
		tmpY_PayUsers := selResponse.RowsResponse[i]["Y_PayUsers"]
		tmpM_PayUsers := selResponse.RowsResponse[i]["M_PayUsers"]
		tmpH_PayUsers := selResponse.RowsResponse[i]["H_PayUsers"]
		tmpNextRegisterUser := selResponse.RowsResponse[i]["NextRegisterUser"]
		tmpNextLoginUser := selResponse.RowsResponse[i]["NextLoginUser"]
		tmpValidNextRegisterUser := selResponse.RowsResponse[i]["ValidNextRegisterUser"]
		tmpValidNextLoginUser := selResponse.RowsResponse[i]["ValidNextLoginUser"]
		tmpSevenRegisterUser := selResponse.RowsResponse[i]["SevenRegisterUser"]
		tmpSevenLoginUser := selResponse.RowsResponse[i]["SevenLoginUser"]
		tmpValidSevenRegisterUser := selResponse.RowsResponse[i]["ValidSevenRegisterUser"]
		tmpValidSevenLoginUser := selResponse.RowsResponse[i]["ValidSevenLoginUser"]
		tmpMonthRegisterUser := selResponse.RowsResponse[i]["MonthRegisterUser"]
		tmpMonthLoginUser := selResponse.RowsResponse[i]["MonthLoginUser"]
		tmpValidMonthRegisterUser := selResponse.RowsResponse[i]["ValidMonthRegisterUser"]
		tmpValidMonthLoginUser := selResponse.RowsResponse[i]["ValidMonthLoginUser"]
		tmpDayNewBetUsers := selResponse.RowsResponse[i]["DayNewBetUsers"]
		tmpdayNewLogin := selResponse.RowsResponse[i]["dayNewLogin"]

		syntax := fmt.Sprintf("('%v', %v, %v, %v, %v, %v, %v, %v, %v, %v, %v, %v, %v, %v, %v, %v, %v, %v, %v, %v, %v, %v, %v, %v, %v, '%v', '%v'),",
			selResponse.RowsResponse[i]["StatisDate"],
			opv.CHKNumVal(tmpChannelId),
			opv.CHKNumVal(tmpY_NonLoginUsers),
			opv.CHKNumVal(tmpM_NonLoginUsers),
			opv.CHKNumVal(tmpH_NonLoginUsers),
			opv.CHKNumVal(tmpY_RegUsers),
			opv.CHKNumVal(tmpM_RegUsers),
			opv.CHKNumVal(tmpH_RegUsers),
			opv.CHKNumVal(tmpY_PayUsers),
			opv.CHKNumVal(tmpM_PayUsers),
			opv.CHKNumVal(tmpH_PayUsers),
			opv.CHKNumVal(tmpNextRegisterUser),
			opv.CHKNumVal(tmpNextLoginUser),
			opv.CHKNumVal(tmpValidNextRegisterUser),
			opv.CHKNumVal(tmpValidNextLoginUser),
			opv.CHKNumVal(tmpSevenRegisterUser),
			opv.CHKNumVal(tmpSevenLoginUser),
			opv.CHKNumVal(tmpValidSevenRegisterUser),
			opv.CHKNumVal(tmpValidSevenLoginUser),
			opv.CHKNumVal(tmpMonthRegisterUser),
			opv.CHKNumVal(tmpMonthLoginUser),
			opv.CHKNumVal(tmpValidMonthRegisterUser),
			opv.CHKNumVal(tmpValidMonthLoginUser),
			opv.CHKNumVal(tmpDayNewBetUsers),
			opv.CHKNumVal(tmpdayNewLogin),
			sourceDB,
			sourceDBSub,
		)

		sb.WriteString(syntax)
	}

	return sb.String()[:len(sb.String())-1] + ";"
}

func QueryStatisusersRNG(lastDate, limit, offset string) string {
	var sb strings.Builder

	sb.WriteString("SELECT * FROM KYStatis.statisusers s ")
	sb.WriteString("WHERE s.StatisDate >= '" + lastDate + "' ")
	sb.WriteString("AND (")
	sb.WriteString(" s.Y_NonLoginUsers <> 0 OR")
	sb.WriteString(" s.M_NonLoginUsers <> 0 OR")
	sb.WriteString(" s.H_NonLoginUsers <> 0 OR")
	sb.WriteString(" s.Y_RegUsers <> 0 OR")
	sb.WriteString(" s.M_RegUsers <> 0 OR")
	sb.WriteString(" s.H_RegUsers <> 0 OR")
	sb.WriteString(" s.Y_PayUsers <> 0 OR")
	sb.WriteString(" s.M_PayUsers <> 0 OR")
	sb.WriteString(" s.H_PayUsers <> 0 OR")
	sb.WriteString(" s.NextRegisterUser <> 0 OR")
	sb.WriteString(" s.NextLoginUser <> 0 OR")
	sb.WriteString(" s.ValidNextRegisterUser <> 0 OR")
	sb.WriteString(" s.ValidNextLoginUser <> 0 OR")
	sb.WriteString(" s.SevenRegisterUser <> 0 OR")
	sb.WriteString(" s.SevenLoginUser <> 0 OR")
	sb.WriteString(" s.ValidSevenRegisterUser <> 0 OR")
	sb.WriteString(" s.ValidSevenLoginUser <> 0 OR")
	sb.WriteString(" s.MonthRegisterUser <> 0 OR")
	sb.WriteString(" s.MonthLoginUser <> 0 OR")
	sb.WriteString(" s.ValidMonthRegisterUser <> 0 OR")
	sb.WriteString(" s.ValidMonthLoginUser <> 0 OR")
	sb.WriteString(" s.DayNewBetUsers <> 0 ")
	// sb.WriteString(" s.dayNewLogin <> 0 OR")
	// sb.WriteString(" s.Y_NewLoginUsers <> 0")
	sb.WriteString(")")
	sb.WriteString("ORDER BY s.StatisDate ASC LIMIT " + limit + ", " + offset + ";")

	return sb.String()
}

func QueryStatisusersLastDate(sourceDB, sourceDBSub string) string {
	var sb strings.Builder

	tblName := fmt.Sprintf(opv.CST_TBL_ASMStatisusers, sourceDB)

	sb.WriteString("SELECT sa.StatisDate AS StatisDate ")
	sb.WriteString("FROM superset." + tblName + " sa ")
	sb.WriteString("WHERE sa.SourceDB = '" + sourceDB + "' ")
	sb.WriteString("AND sa.SourceDBSub = '" + sourceDBSub + "' ")
	sb.WriteString("AND sa.ChannelId <> -1 ")
	sb.WriteString("ORDER BY sa.StatisDate DESC ")
	sb.WriteString("LIMIT 1;")

	return sb.String()
}
