package asm

import (
	"fmt"
	"time"

	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
	cf "gitlab.com/sw.weizhen/project.sups.sched/module/conf"
	sq "gitlab.com/sw.weizhen/project.sups.sched/task/asm/sqlscript"
	db "gitlab.com/sw.weizhen/rdbms.mysql"
)

func replaceSupersetStatisRecordAgentAll(sourceDB, sourceDBSub string, selResponse *db.DBResponse, dbdesptr *db.DBOperator) error {
	sqlSyntax := sq.ReplaceAgentAll(sourceDB, sourceDBSub, selResponse)
	_, err := dbdesptr.Exec(sqlSyntax)

	return err
}

func batchStatisRecordAgentAll(head string, batch int, dbsrcCfg cf.DBSrcCfg, dbdesPtr, dbsrcPtr *db.DBOperator) {

	i := 0
	for {
		respStatisusers, err := dbsrcPtr.Query(sq.QueryAgentAllRNG(head, fmt.Sprintf("%d", i), fmt.Sprintf("%d", batch)))
		if err != nil {
			opv.L.Error("%s(%s)::batch::QueryAgentAllRNG, err -> %v", dbsrcCfg.Domain, dbsrcCfg.Hostname, err)
			break
		}

		if respStatisusers.Length == 0 {
			break
		}

		err = replaceSupersetStatisRecordAgentAll(dbsrcCfg.Domain, dbsrcCfg.Hostname, respStatisusers, dbdesPtr)
		if err != nil {
			opv.L.Error("%s(%s)::batch::replaceSupersetStatisRecordAgentAllData, err -> %v", dbsrcCfg.Domain, dbsrcCfg.Hostname, err)
			break
		}

		if respStatisusers.Length < uint32(batch) {
			break
		}

		i = i + batch
	}

}

func asmStatisRecordAgentAll(fromLastDate bool, head string, batch int, dbAsmCfg cf.DBSrcCfg, dbdesptr, dbasmptr *db.DBOperator) error {

	ctHead := head

	if fromLastDate {
		respLastData, err := dbdesptr.Query(sq.QueryAgentAllLastDate(dbAsmCfg.Domain, dbAsmCfg.Hostname))
		if err != nil {
			return err
		}

		if respLastData.Length != 0 {
			ctHead = respLastData.RowsResponse[0]["StatisDate"]
		}
	}

	t1 := time.Now().Unix()

	batchStatisRecordAgentAll(ctHead, batch, dbAsmCfg, dbdesptr, dbasmptr)

	cost := opv.FMTSecToHour(time.Now().Unix() - t1)

	opv.L.Info("asm done => agent_all: %s(%s, %s) -> cost: %s", dbAsmCfg.Domain, dbAsmCfg.Hostname, ctHead, cost)

	return nil
}
