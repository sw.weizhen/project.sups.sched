package sum

import (
	"time"

	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
	cf "gitlab.com/sw.weizhen/project.sups.sched/module/conf"
	sq "gitlab.com/sw.weizhen/project.sups.sched/task/sum/sqlscript"
	db "gitlab.com/sw.weizhen/rdbms.mysql"
	ct "gitlab.com/sw.weizhen/util.calc.timer"
)

func sumBrandAgentAllPerDay(date string, dbsrcCfg cf.DBSrcCfg, dbdesptr, dbsrcptr *db.DBOperator) {

	respDataSum, err := dbsrcptr.Query(sq.QueryAgentAllSumPerDay(date))
	if err != nil {
		opv.L.Error("%s(%s)::sum::QueryAgentAllSumPerDay, err -> %v", dbsrcCfg.Domain, dbsrcCfg.Hostname, err)

		return
	}

	if respDataSum.Length <= 0 {
		return
	}

	sqlSyntax := sq.ReplaceBrandAgentAll(date, dbsrcCfg.Domain, dbsrcCfg.Hostname, respDataSum)
	_, err = dbdesptr.Exec(sqlSyntax)
	if err != nil {
		opv.L.Error("%s(%s)::sum::ReplaceBrandAgentAll, err -> %v", dbsrcCfg.Domain, dbsrcCfg.Hostname, err)
		return
	}
}

func sumBrandAgentAll(fromLastDate bool, head string, dbsrcCfg cf.DBSrcCfg, dbdesptr, dbsrcptr *db.DBOperator) error {

	ctHead := head

	if fromLastDate {
		lastDate, err := dbdesptr.Query(sq.QueryBrandLastDate(
			opv.CST_TBL_SumBrandAgentAll,
			dbsrcCfg.Domain,
			dbsrcCfg.Hostname,
		))

		if err != nil {
			return err
		}

		if lastDate.Length != 0 {
			ctHead = lastDate.RowsResponse[0]["StatisDate"]
		}
	}

	lsDate, err := ct.New(
		ct.OptInput(ctHead),
		ct.OptFMTinput(ct.CST_FMT_YMD),
	).CALCTimeline(
		ct.OptINVLMode(ct.ENUM_INVL_DAY),
		ct.OptOutFMT(ct.CST_FMT_YMD),
		ct.OptLimitTimeOverlap(false),
	)

	if err != nil {
		return err
	}

	t1 := time.Now().Unix()

	for _, date := range lsDate {
		sumBrandAgentAllPerDay(date, dbsrcCfg, dbdesptr, dbsrcptr)
	}

	cost := opv.FMTSecToHour(time.Now().Unix() - t1)

	opv.L.Info("sum done => agent_all: %s(%s, %s) -> cost: %s", dbsrcCfg.Domain, dbsrcCfg.Hostname, ctHead, cost)

	return nil
}
