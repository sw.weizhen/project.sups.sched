package sqlscript

import (
	"fmt"
	"strings"

	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
	swmysql "gitlab.com/sw.weizhen/rdbms.mysql"
)

func CreateTBLAgentGame() string {
	var sb strings.Builder

	sb.WriteString("CREATE TABLE IF NOT EXISTS `" + opv.CST_TBL_SumBrandAgentGame + "` (")
	sb.WriteString(" `StatisDate` date NOT NULL,")
	sb.WriteString(" `WinGold` bigint(20) DEFAULT NULL,")
	sb.WriteString(" `LostGold` bigint(20) DEFAULT NULL,")
	sb.WriteString(" `CellScore` bigint(20) DEFAULT NULL,")
	sb.WriteString(" `Revenue` bigint(20) DEFAULT NULL,")
	sb.WriteString(" `WinNum` int(11) DEFAULT NULL,")
	sb.WriteString(" `LostNum` int(11) DEFAULT NULL,")
	sb.WriteString(" `ActiveUsers` int(11) DEFAULT NULL,")
	sb.WriteString(" `Brand` varchar(20) NOT NULL,")
	sb.WriteString(" `SourceDBSub` varchar(20) NOT NULL,")
	sb.WriteString(" `MoveDate` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,")
	sb.WriteString("PRIMARY KEY (`StatisDate`, `Brand`, `SourceDBSub`)")
	sb.WriteString(") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;")

	return sb.String()
}

func QueryAgentGameSumPerDay(date string) string {
	var sb strings.Builder

	sb.WriteString("SELECT")
	sb.WriteString(" srag.StatisDate,")
	sb.WriteString(" SUM(srag.WinGold) AS WinGold,")
	sb.WriteString(" SUM(srag.LostGold) AS LostGold,")
	sb.WriteString(" SUM(srag.CellScore) AS CellScore,")
	sb.WriteString(" SUM(srag.Revenue) AS Revenue,")
	sb.WriteString(" SUM(srag.WinNum) AS WinNum,")
	sb.WriteString(" SUM(srag.LostNum) AS LostNum,")
	sb.WriteString(" SUM(srag.ActiveUsers) AS ActiveUsers ")
	sb.WriteString("FROM KYStatis.statis_record_agent_game srag ")
	sb.WriteString("WHERE srag.StatisDate = '" + date + "';")

	return sb.String()
}

func ReplaceBrandAgentGame(date, brand, sourceDBSub string, selResponse *swmysql.DBResponse) string {

	var sb strings.Builder

	sb.WriteString("REPLACE INTO superset." + opv.CST_TBL_SumBrandAgentGame + " (")
	sb.WriteString(" StatisDate,")
	sb.WriteString(" WinGold,")
	sb.WriteString(" LostGold,")
	sb.WriteString(" CellScore,")
	sb.WriteString(" Revenue,")
	sb.WriteString(" WinNum,")
	sb.WriteString(" LostNum,")
	sb.WriteString(" ActiveUsers,")
	sb.WriteString(" Brand,")
	sb.WriteString(" SourceDBSub")
	sb.WriteString(")")
	sb.WriteString("VALUES")

	for i := 0; i < int(selResponse.Length); i++ {

		tmpSumWinGold := selResponse.RowsResponse[i]["WinGold"]
		tmpSumLostGold := selResponse.RowsResponse[i]["LostGold"]
		tmpSumCellScore := selResponse.RowsResponse[i]["CellScore"]
		tmpSumRevenue := selResponse.RowsResponse[i]["Revenue"]
		tmpSumWinNum := selResponse.RowsResponse[i]["WinNum"]
		tmpSumLostNum := selResponse.RowsResponse[i]["LostNum"]
		tmpSumActiveUsers := selResponse.RowsResponse[i]["ActiveUsers"]

		syntax := fmt.Sprintf("('%v', %v, %v, %v, %v, %v, %v, %v, '%v', '%v'),",
			date,
			opv.CHKNumVal(tmpSumWinGold),
			opv.CHKNumVal(tmpSumLostGold),
			opv.CHKNumVal(tmpSumCellScore),
			opv.CHKNumVal(tmpSumRevenue),
			opv.CHKNumVal(tmpSumWinNum),
			opv.CHKNumVal(tmpSumLostNum),
			opv.CHKNumVal(tmpSumActiveUsers),
			brand,
			sourceDBSub,
		)

		sb.WriteString(syntax)
	}

	return sb.String()[:len(sb.String())-1] + ";"
}
