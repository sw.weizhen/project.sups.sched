package sqlscript

import (
	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
	swmysql "gitlab.com/sw.weizhen/rdbms.mysql"

	"fmt"
	"strings"
)

func CreateTBLStatisusers() string {
	var sb strings.Builder

	sb.WriteString("CREATE TABLE IF NOT EXISTS `" + opv.CST_TBL_SumBrandStatisusers + "` (")
	sb.WriteString(" `StatisDate` date NOT NULL,")
	sb.WriteString(" `Y_NonLoginUsers` int(11) DEFAULT NULL,")
	sb.WriteString(" `M_NonLoginUsers` int(11) DEFAULT NULL,")
	sb.WriteString(" `H_NonLoginUsers` int(11) DEFAULT NULL,")
	sb.WriteString(" `Y_RegUsers` int(11) DEFAULT NULL,")
	sb.WriteString(" `M_RegUsers` int(11) DEFAULT NULL,")
	sb.WriteString(" `H_RegUsers` int(11) DEFAULT NULL,")
	sb.WriteString(" `Y_PayUsers` int(11) NOT NULL,")
	sb.WriteString(" `M_PayUsers` int(11) DEFAULT NULL,")
	sb.WriteString(" `H_PayUsers` int(11) DEFAULT NULL,")
	sb.WriteString(" `NextRegisterUser` int(11) NOT NULL,")
	sb.WriteString(" `NextLoginUser` int(11) DEFAULT NULL,")
	sb.WriteString(" `ValidNextRegisterUser` int(11) DEFAULT NULL,")
	sb.WriteString(" `ValidNextLoginUser` int(11) DEFAULT NULL,")
	sb.WriteString(" `SevenRegisterUser` int(11) DEFAULT NULL,")
	sb.WriteString(" `SevenLoginUser` int(11) DEFAULT NULL,")
	sb.WriteString(" `ValidSevenRegisterUser` int(11) DEFAULT NULL,")
	sb.WriteString(" `ValidSevenLoginUser` int(11) DEFAULT NULL,")
	sb.WriteString(" `MonthRegisterUser` int(11) DEFAULT NULL,")
	sb.WriteString(" `MonthLoginUser` int(11) DEFAULT NULL,")
	sb.WriteString(" `ValidMonthRegisterUser` int(11) DEFAULT NULL,")
	sb.WriteString(" `ValidMonthLoginUser` int(11) DEFAULT NULL,")
	sb.WriteString(" `DayNewBetUsers` int(11) DEFAULT NULL,")
	sb.WriteString(" `dayNewLogin` int(11) DEFAULT NULL,")
	sb.WriteString(" `Brand` varchar(20) NOT NULL,")
	sb.WriteString(" `SourceDBSub` varchar(20) NOT NULL,")
	sb.WriteString(" `MoveDate` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,")
	sb.WriteString(" PRIMARY KEY (`StatisDate`, `Brand`, `SourceDBSub`)")
	sb.WriteString(") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;")

	return sb.String()
}

func QueryStatisusersPerDay(date string) string {
	var sb strings.Builder

	sb.WriteString("SELECT")
	sb.WriteString(" s.StatisDate AS StatisDate,")
	sb.WriteString(" SUM(s.Y_NonLoginUsers) AS Y_NonLoginUsers,")
	sb.WriteString(" SUM(s.M_NonLoginUsers) AS M_NonLoginUsers,")
	sb.WriteString(" SUM(s.H_NonLoginUsers) AS H_NonLoginUsers,")
	sb.WriteString(" SUM(s.Y_RegUsers) AS Y_RegUsers,")
	sb.WriteString(" SUM(s.M_RegUsers) AS M_RegUsers,")
	sb.WriteString(" SUM(s.H_RegUsers) AS H_RegUsers,")
	sb.WriteString(" SUM(s.Y_PayUsers) AS Y_PayUsers,")
	sb.WriteString(" SUM(s.M_PayUsers) AS M_PayUsers,")
	sb.WriteString(" SUM(s.H_PayUsers) AS H_PayUsers,")
	sb.WriteString(" SUM(s.NextRegisterUser) AS NextRegisterUser,")
	sb.WriteString(" SUM(s.NextLoginUser) AS NextLoginUser,")
	sb.WriteString(" SUM(s.ValidNextRegisterUser) AS ValidNextRegisterUser,")
	sb.WriteString(" SUM(s.ValidNextLoginUser) AS ValidNextLoginUser,")
	sb.WriteString(" SUM(s.SevenRegisterUser) AS SevenRegisterUser,")
	sb.WriteString(" SUM(s.SevenLoginUser) AS SevenLoginUser,")
	sb.WriteString(" SUM(s.ValidSevenRegisterUser) AS ValidSevenRegisterUser,")
	sb.WriteString(" SUM(s.ValidSevenLoginUser) AS ValidSevenLoginUser,")
	sb.WriteString(" SUM(s.MonthRegisterUser) AS MonthRegisterUser,")
	sb.WriteString(" SUM(s.MonthLoginUser) AS MonthLoginUser,")
	sb.WriteString(" SUM(s.ValidMonthRegisterUser) AS ValidMonthRegisterUser,")
	sb.WriteString(" SUM(s.ValidMonthLoginUser) AS ValidMonthLoginUser,")
	sb.WriteString(" SUM(s.DayNewBetUsers) AS DayNewBetUsers ")
	sb.WriteString("FROM KYStatis.statisusers s ")
	sb.WriteString("WHERE s.StatisDate = '" + date + "';")

	return sb.String()
}

func ReplaceBrandStatisusers(date, brand, sourceDBSub string, selResponse *swmysql.DBResponse) string {
	var sb strings.Builder

	sb.WriteString("REPLACE INTO superset." + opv.CST_TBL_SumBrandStatisusers + " (")
	sb.WriteString(" StatisDate, ")
	sb.WriteString(" Y_NonLoginUsers, ")
	sb.WriteString(" M_NonLoginUsers, ")
	sb.WriteString(" H_NonLoginUsers, ")
	sb.WriteString(" Y_RegUsers, ")
	sb.WriteString(" M_RegUsers, ")
	sb.WriteString(" H_RegUsers, ")
	sb.WriteString(" Y_PayUsers, ")
	sb.WriteString(" M_PayUsers, ")
	sb.WriteString(" H_PayUsers, ")
	sb.WriteString(" NextRegisterUser, ")
	sb.WriteString(" NextLoginUser, ")
	sb.WriteString(" ValidNextRegisterUser, ")
	sb.WriteString(" ValidNextLoginUser, ")
	sb.WriteString(" SevenRegisterUser, ")
	sb.WriteString(" SevenLoginUser, ")
	sb.WriteString(" ValidSevenRegisterUser, ")
	sb.WriteString(" ValidSevenLoginUser, ")
	sb.WriteString(" MonthRegisterUser, ")
	sb.WriteString(" MonthLoginUser, ")
	sb.WriteString(" ValidMonthRegisterUser, ")
	sb.WriteString(" ValidMonthLoginUser, ")
	sb.WriteString(" DayNewBetUsers, ")
	sb.WriteString(" dayNewLogin, ")
	sb.WriteString(" Brand, ")
	sb.WriteString(" SourceDBSub")
	sb.WriteString(")")
	sb.WriteString("VALUES")

	for i := 0; i < int(selResponse.Length); i++ {

		tmpY_NonLoginUsers := selResponse.RowsResponse[i]["Y_NonLoginUsers"]
		tmpM_NonLoginUsers := selResponse.RowsResponse[i]["M_NonLoginUsers"]
		tmpH_NonLoginUsers := selResponse.RowsResponse[i]["H_NonLoginUsers"]
		tmpY_RegUsers := selResponse.RowsResponse[i]["Y_RegUsers"]
		tmpM_RegUsers := selResponse.RowsResponse[i]["M_RegUsers"]
		tmpH_RegUsers := selResponse.RowsResponse[i]["H_RegUsers"]
		tmpY_PayUsers := selResponse.RowsResponse[i]["Y_PayUsers"]
		tmpM_PayUsers := selResponse.RowsResponse[i]["M_PayUsers"]
		tmpH_PayUsers := selResponse.RowsResponse[i]["H_PayUsers"]
		tmpNextRegisterUser := selResponse.RowsResponse[i]["NextRegisterUser"]
		tmpNextLoginUser := selResponse.RowsResponse[i]["NextLoginUser"]
		tmpValidNextRegisterUser := selResponse.RowsResponse[i]["ValidNextRegisterUser"]
		tmpValidNextLoginUser := selResponse.RowsResponse[i]["ValidNextLoginUser"]
		tmpSevenRegisterUser := selResponse.RowsResponse[i]["SevenRegisterUser"]
		tmpSevenLoginUser := selResponse.RowsResponse[i]["SevenLoginUser"]
		tmpValidSevenRegisterUser := selResponse.RowsResponse[i]["ValidSevenRegisterUser"]
		tmpValidSevenLoginUser := selResponse.RowsResponse[i]["ValidSevenLoginUser"]
		tmpMonthRegisterUser := selResponse.RowsResponse[i]["MonthRegisterUser"]
		tmpMonthLoginUser := selResponse.RowsResponse[i]["MonthLoginUser"]
		tmpValidMonthRegisterUser := selResponse.RowsResponse[i]["ValidMonthRegisterUser"]
		tmpValidMonthLoginUser := selResponse.RowsResponse[i]["ValidMonthLoginUser"]
		tmpDayNewBetUsers := selResponse.RowsResponse[i]["DayNewBetUsers"]
		tmpdayNewLogin := selResponse.RowsResponse[i]["dayNewLogin"]

		syntax := fmt.Sprintf("('%v', %v, %v, %v, %v, %v, %v, %v, %v, %v, %v, %v, %v, %v, %v, %v, %v, %v, %v, %v, %v, %v, %v, %v, '%v', '%v'),",
			date,
			opv.CHKNumVal(tmpY_NonLoginUsers),
			opv.CHKNumVal(tmpM_NonLoginUsers),
			opv.CHKNumVal(tmpH_NonLoginUsers),
			opv.CHKNumVal(tmpY_RegUsers),
			opv.CHKNumVal(tmpM_RegUsers),
			opv.CHKNumVal(tmpH_RegUsers),
			opv.CHKNumVal(tmpY_PayUsers),
			opv.CHKNumVal(tmpM_PayUsers),
			opv.CHKNumVal(tmpH_PayUsers),
			opv.CHKNumVal(tmpNextRegisterUser),
			opv.CHKNumVal(tmpNextLoginUser),
			opv.CHKNumVal(tmpValidNextRegisterUser),
			opv.CHKNumVal(tmpValidNextLoginUser),
			opv.CHKNumVal(tmpSevenRegisterUser),
			opv.CHKNumVal(tmpSevenLoginUser),
			opv.CHKNumVal(tmpValidSevenRegisterUser),
			opv.CHKNumVal(tmpValidSevenLoginUser),
			opv.CHKNumVal(tmpMonthRegisterUser),
			opv.CHKNumVal(tmpMonthLoginUser),
			opv.CHKNumVal(tmpValidMonthRegisterUser),
			opv.CHKNumVal(tmpValidMonthLoginUser),
			opv.CHKNumVal(tmpDayNewBetUsers),
			opv.CHKNumVal(tmpdayNewLogin),
			brand,
			sourceDBSub,
		)

		sb.WriteString(syntax)
	}

	return sb.String()[:len(sb.String())-1] + ";"
}
