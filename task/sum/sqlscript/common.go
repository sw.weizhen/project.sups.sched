package sqlscript

import (
	"strings"
)

func QueryBrandLastDate(tbl, brand, sourceDB string) string {
	var sb strings.Builder

	sb.WriteString("SELECT sbs.StatisDate AS StatisDate ")
	sb.WriteString("FROM superset." + tbl + " sbs ")
	sb.WriteString("WHERE sbs.Brand = '" + brand + "' ")
	sb.WriteString("AND sbs.SourceDBSub = '" + sourceDB + "' ")
	sb.WriteString("ORDER BY sbs.StatisDate DESC ")
	sb.WriteString("LIMIT 1;")

	return sb.String()
}
