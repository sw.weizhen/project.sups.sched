package sum

import (
	"sync"

	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
	cf "gitlab.com/sw.weizhen/project.sups.sched/module/conf"
)

func Recall() {

	if err := prepareTBL(); err != nil {
		opv.L.Error("sum::prepareTBL failed, err -> %v", err)
		return
	}

	var wgASM sync.WaitGroup
	for i := 0; i < len(opv.CFG.RDBCfg.Source); i++ {
		sumCfgdb := opv.CFG.RDBCfg.Source[i]

		head := sumCfgdb.Head.SUM

		if sumCfgdb.Enable && len(head) > 0 {
			wgASM.Add(1)
			go func(dbsrcCfg cf.DBSrcCfg) {
				if err := recall(false, head, opv.CFG.CaptureCfg.Batch, dbsrcCfg); err != nil {
					opv.L.Error("SUM::Recall(%s, %s:%d), err -> %v", sumCfgdb.Hostname, sumCfgdb.Host, sumCfgdb.Port, err)
				}

				wgASM.Done()

			}(sumCfgdb)
		}
	}

	wgASM.Wait()

	opv.L.Info("SUM DONE")

}
