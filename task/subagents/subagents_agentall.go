package subagents

import (
	"fmt"
	"strings"
	"time"

	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
	cf "gitlab.com/sw.weizhen/project.sups.sched/module/conf"
	sq "gitlab.com/sw.weizhen/project.sups.sched/task/subagents/sqlscript"
	db "gitlab.com/sw.weizhen/rdbms.mysql"
)

func ComputeAgentAllBrandPerDay(date string, agentSet []string, dbsrcCfg cf.DBSrcCfg, dbdesptr *db.DBOperator) {

	for _, agents := range agentSet {
		roots := strings.Split(agents, ",")
		if len(roots) <= 0 {
			continue
		}

		rootChl := roots[0]

		respData, err := dbdesptr.Query(sq.QuerySupersetStatisRecordAgentAllASMPerAgent(date, dbsrcCfg.Domain, agents))
		if err != nil {
			opv.L.Error("%s(%s)::subagents::ComputeAgentAllBrandPerDay::QuerySupersetStatisRecordAgentAllASMPerAgent, err -> %v", dbsrcCfg.Domain, dbsrcCfg.Hostname, err)
			continue
		}

		if respData.Length <= 0 {
			opv.L.Error("%s(%s)::subagents::ComputeAgentAllBrandPerDay::QuerySupersetStatisRecordAgentAllASMPerAgent, err -> %v no found data", date)
			continue
		}

		data := respData.RowsResponse[0]
		if len(data) <= 0 {
			continue
		}

		_, err = dbdesptr.Exec(sq.ReplaceAgentAll(date, rootChl, opv.CST_TBL_SubagentBrandAgentAll, dbsrcCfg.Domain, dbsrcCfg.Hostname, respData))
		if err != nil {
			opv.L.Error("%s(%s)::subagents::ComputeAgentAllBrandPerDay::ReplaceAgentAll, err -> %v", dbsrcCfg.Domain, dbsrcCfg.Hostname, err)
			continue
		}

		subTBL := fmt.Sprintf(opv.CST_TBL_SubagentDBNAgentAll, dbsrcCfg.Domain, dbsrcCfg.Hostname)
		_, err = dbdesptr.Exec(sq.ReplaceAgentAll(date, rootChl, subTBL, dbsrcCfg.Domain, dbsrcCfg.Hostname, respData))
		if err != nil {
			opv.L.Error("%s(%s)::subagents::ComputeAgentAllBrandPerDay::ReplaceAgentAll(Sub), err -> %v", dbsrcCfg.Domain, dbsrcCfg.Hostname, err)
			continue
		}

	}

}

func subagentsBrandAll(fromLastDate bool, head string, dbsrcCfg cf.DBSrcCfg, dbdesptr, dbsrcptr *db.DBOperator) error {

	days, err := prepareAgentAllDays(fromLastDate, head, dbsrcCfg, dbdesptr)
	if err != nil {
		return fmt.Errorf("%s(%s)::subagents::subagentsBrandAll::prepareAgentAllDays, err -> %v", dbsrcCfg.Domain, dbsrcCfg.Hostname, err)
	}

	roots, err := PrepareAgentRoots(dbsrcCfg, dbsrcptr)
	if err != nil {
		return fmt.Errorf("%s(%s)::subagents::subagentsBrandAll::prepareAgentRoots, err -> %v", dbsrcCfg.Domain, dbsrcCfg.Hostname, err)
	}

	agentSet := RootMapToSliceStr(roots)
	if len(agentSet) == 0 {
		return fmt.Errorf("%s(%s)::subagents::subagentsBrandAll::rootMapToSliceStr, err -> empty subagent container", dbsrcCfg.Domain, dbsrcCfg.Hostname)
	}

	t1 := time.Now().Unix()

	for _, date := range days {
		ComputeAgentAllBrandPerDay(date, agentSet, dbsrcCfg, dbdesptr)
	}

	cost := opv.FMTSecToHour(time.Now().Unix() - t1)

	opv.L.Info("subagents done => agent_all: %s(%s, %s) -> cost: %s", dbsrcCfg.Domain, dbsrcCfg.Hostname, days[0], cost)

	return nil
}
