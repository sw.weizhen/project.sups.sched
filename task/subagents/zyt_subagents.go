package subagents

import (
	"sync"

	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
	cf "gitlab.com/sw.weizhen/project.sups.sched/module/conf"
)

func Recall() {

	if err := prepareTBL(); err != nil {
		opv.L.Error("subagents::prepareTBL failed, err -> %v", err)
		return
	}

	var wgASM sync.WaitGroup
	for i := 0; i < len(opv.CFG.RDBCfg.Source); i++ {
		subagentsCfgdb := opv.CFG.RDBCfg.Source[i]

		head := subagentsCfgdb.Head.Subagent

		if subagentsCfgdb.Enable && len(head) > 0 {
			wgASM.Add(1)
			go func(dbsrcCfg cf.DBSrcCfg) {
				if err := recall(false, head, opv.CFG.CaptureCfg.Batch, dbsrcCfg); err != nil {
					opv.L.Error("SUBAGENTS::Recall(%s, %s:%d), err -> %v", subagentsCfgdb.Hostname, subagentsCfgdb.Host, subagentsCfgdb.Port, err)
				}

				wgASM.Done()

			}(subagentsCfgdb)
		}
	}

	wgASM.Wait()

	opv.L.Info("SUBAGENTS DONE")
}
