package subagents

import (
	"fmt"

	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
	cf "gitlab.com/sw.weizhen/project.sups.sched/module/conf"
	sq "gitlab.com/sw.weizhen/project.sups.sched/task/subagents/sqlscript"
	db "gitlab.com/sw.weizhen/rdbms.mysql"
	ct "gitlab.com/sw.weizhen/util.calc.timer"
)

// func msgAgent(roots map[string]*[]string) string {
// 	var msg strings.Builder

// 	msg.WriteString(fmt.Sprintf("roots count: %v\n", len(roots)))

// 	for root, v := range roots {

// 		subAgentCount := len(*v)

// 		msg.WriteString(fmt.Sprintf("root: %s", root))

// 		if subAgentCount > 0 {
// 			msg.WriteString(fmt.Sprintf(" -> %d branch(s): %v", len(*v), *v))
// 		}

// 		msg.WriteString("\n")
// 	}

// 	return msg.String()[:len(msg.String())-1]
// }

// func bs(head, tail int, goal string, slice []string) int {
// 	if head > tail {
// 		return -1
// 	}

// 	poe := (head + tail) / 2

// 	val := slice[poe]

// 	if val == goal {
// 		return poe
// 	}

// 	if val > goal {
// 		return bs(head, poe-1, goal, slice)

// 	} else {
// 		return bs(poe+1, tail, goal, slice)

// 	}
// }

func childrenChannel(parent string, agentData []map[string]string) []string {

	children := make([]string, 0)
	for _, channel := range agentData {
		if channel["parent"] == parent {
			children = append(children, channel["agent"])
		}
	}

	return children
}

func subsetChannel(parent string, agentData []map[string]string, container *[]string) {

	childrenChl := childrenChannel(parent, agentData)
	if len(childrenChl) == 0 {
		return
	}

	*container = append(*container, childrenChl...)

	for _, channel := range childrenChl {
		subsetChannel(channel, agentData, container)
	}

}

func rootChannel(agentData []map[string]string) map[string]*[]string {
	roots := make(map[string]*[]string, 0)

	for _, channel := range agentData {
		root := channel["parent"]
		if root == "0" {
			roots[channel["agent"]] = new([]string)
		}
	}

	return roots
}

func RootMapToSliceStr(roots map[string]*[]string) []string {

	agentSet := make([]string, 0)

	for parent, channels := range roots {
		agentChannel := parent
		for _, channel := range *channels {
			agentChannel += ", " + channel
		}

		agentSet = append(agentSet, agentChannel)
	}

	return agentSet
}

func prepareReguserDays(fromLastDate bool, head string, dbsrcCfg cf.DBSrcCfg, dbdesptr *db.DBOperator) ([]string, error) {
	ctHead := head
	if fromLastDate {
		respLastData, err := dbdesptr.Query(sq.QueryBrandSupersetReguserLastDate(dbsrcCfg.Domain, dbsrcCfg.Hostname))
		if err != nil {
			return nil, err
		}

		if respLastData.Length != 0 {
			ctHead = respLastData.RowsResponse[0]["StatisDate"]
		}
	}

	tl, err := ct.New(
		ct.OptInput(ctHead),
		ct.OptFMTinput(ct.CST_FMT_YMD),
	).CALCTimeline(
		ct.OptINVLMode(ct.ENUM_INVL_DAY),
		ct.OptOutFMT(ct.CST_FMT_YMD),
	)

	if err != nil {
		return nil, err
	}

	return tl, nil
}

func prepareAgentAllDays(fromLastDate bool, head string, dbsrcCfg cf.DBSrcCfg, dbdesptr *db.DBOperator) ([]string, error) {
	ctHead := head
	if fromLastDate {
		respLastData, err := dbdesptr.Query(sq.QueryBrandSupersetAgentAllLastDate(dbsrcCfg.Domain, dbsrcCfg.Hostname))
		if err != nil {
			return nil, err
		}

		if respLastData.Length != 0 {
			ctHead = respLastData.RowsResponse[0]["StatisDate"]
		}
	}

	tl, err := ct.New(
		ct.OptInput(ctHead),
		ct.OptFMTinput(ct.CST_FMT_YMD),
	).CALCTimeline(
		ct.OptINVLMode(ct.ENUM_INVL_DAY),
		ct.OptOutFMT(ct.CST_FMT_YMD),
	)

	if err != nil {
		return nil, err
	}

	return tl, nil
}

func prepareDays(fromLastDate bool, head string, dbsrcCfg cf.DBSrcCfg, dbdesptr *db.DBOperator) ([]string, error) {

	ctHead := head

	if fromLastDate {
		respLastData, err := dbdesptr.Query(sq.QueryBrandSupersetAgentGameLastDate(dbsrcCfg.Domain, dbsrcCfg.Hostname))
		if err != nil {
			return nil, err
		}

		if respLastData.Length != 0 {
			ctHead = respLastData.RowsResponse[0]["StatisDate"]
		}
	}

	tl, err := ct.New(
		ct.OptInput(ctHead),
		ct.OptFMTinput(ct.CST_FMT_YMD),
	).CALCTimeline(
		ct.OptINVLMode(ct.ENUM_INVL_DAY),
		ct.OptOutFMT(ct.CST_FMT_YMD),
	)

	if err != nil {
		return nil, err
	}

	return tl, nil
}

func prepareStatisusersDays(fromLastDate bool, head string, dbsrcCfg cf.DBSrcCfg, dbdesptr *db.DBOperator) ([]string, error) {
	ctHead := head
	if fromLastDate {
		respLastData, err := dbdesptr.Query(sq.QueryBrandSupersetStatisusersLastDate(dbsrcCfg.Domain, dbsrcCfg.Hostname))
		if err != nil {
			return nil, err
		}

		if respLastData.Length != 0 {
			ctHead = respLastData.RowsResponse[0]["StatisDate"]
		}
	}

	tl, err := ct.New(
		ct.OptInput(ctHead),
		ct.OptFMTinput(ct.CST_FMT_YMD),
	).CALCTimeline(
		ct.OptINVLMode(ct.ENUM_INVL_DAY),
		ct.OptOutFMT(ct.CST_FMT_YMD),
	)

	if err != nil {
		return nil, err
	}

	return tl, nil
}

func batchAgents(batch int, dbsrcCfg cf.DBSrcCfg, dbsrcptr *db.DBOperator) []map[string]string {

	var agentsData []map[string]string = make([]map[string]string, 0)

	i := 0

	for {
		syntax := sq.QueryGameAPIAgentsRNG(fmt.Sprintf("%d", i), fmt.Sprintf("%d", batch))
		respAgents, err := dbsrcptr.Query(syntax)
		if err != nil {
			opv.L.Error("%s(%s)::subagents::QueryGameAPIAgentsRNG, err -> %v", dbsrcCfg.Domain, dbsrcCfg.Hostname, err)

			break
		}

		if respAgents.Length == 0 {
			break
		}

		agentsData = append(agentsData, respAgents.RowsResponse...)

		if respAgents.Length < uint32(batch) {
			break
		}

		i = i + batch
	}

	return agentsData
}

func PrepareAgentRoots(dbsrcCfg cf.DBSrcCfg, dbsrcptr *db.DBOperator) (map[string]*[]string, error) {
	batch := opv.CFG.CaptureCfg.Batch

	agentData := batchAgents(batch, dbsrcCfg, dbsrcptr)
	if len(agentData) == 0 {
		return nil, fmt.Errorf("%s(%s)::PrepareAgentRoots(agents), no found agents", dbsrcCfg.Domain, dbsrcCfg.Hostname)
	}

	roots := rootChannel(agentData)

	for root, container := range roots {
		subsetChannel(root, agentData, container)
	}

	return roots, nil
}

func prepareAllGMs(statisDate, domain string, dbsrcptr *db.DBOperator) ([]string, error) {
	respGMs, err := dbsrcptr.Query(sq.QuerySupersetStatisRecordAgentGameAllGame(statisDate, domain))
	if err != nil {
		return nil, err
	}

	if respGMs.Length == 0 {
		return nil, fmt.Errorf("%s no found games", statisDate)
	}

	data := make([]string, respGMs.Length)
	for i, gm := range respGMs.RowsResponse {
		data[i] = gm["GameID"]
	}

	return data, nil
}
