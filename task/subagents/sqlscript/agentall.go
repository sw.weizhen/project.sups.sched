package sqlscript

import (
	"fmt"
	"strings"

	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
	swmysql "gitlab.com/sw.weizhen/rdbms.mysql"
)

func CreateTBLAgentAll(tbl string) string {
	var sb strings.Builder

	sb.WriteString("CREATE TABLE IF NOT EXISTS `" + tbl + "` (")
	sb.WriteString(" `StatisDate` date NOT NULL,")
	sb.WriteString(" `RootChannel` int(11) NOT NULL,")
	sb.WriteString(" `WinGold` bigint(20) DEFAULT NULL,")
	sb.WriteString(" `LostGold` bigint(20) DEFAULT NULL,")
	sb.WriteString(" `CellScore` bigint(20) DEFAULT NULL,")
	sb.WriteString(" `Revenue` bigint(20) DEFAULT NULL,")
	sb.WriteString(" `WinNum` int(11) DEFAULT NULL,")
	sb.WriteString(" `LostNum` int(11) DEFAULT NULL,")
	sb.WriteString(" `ActiveUsers` int(11) DEFAULT NULL,")
	sb.WriteString(" `Subset` MEDIUMTEXT DEFAULT NULL,")
	sb.WriteString(" `SourceDB` varchar(20) NOT NULL,")
	sb.WriteString(" `SourceDBSub` varchar(20) NOT NULL,")
	sb.WriteString(" `MoveDate` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,")
	sb.WriteString(" PRIMARY KEY (`StatisDate`,`RootChannel`, `SourceDB`, `SourceDBSub`)")
	sb.WriteString(" ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;")

	return sb.String()
}

func QueryBrandSupersetAgentAllLastDate(sourceDB, sourceDBSub string) string {
	var sb strings.Builder

	sb.WriteString("SELECT stbl.StatisDate AS StatisDate ")
	sb.WriteString("FROM superset." + opv.CST_TBL_SubagentBrandAgentAll + " stbl ")
	sb.WriteString("WHERE stbl.SourceDB = '" + sourceDB + "' ")
	sb.WriteString("AND stbl.SourceDBSub = '" + sourceDBSub + "' ")
	sb.WriteString("AND stbl.RootChannel <> -1 ")
	sb.WriteString("ORDER BY stbl.StatisDate DESC ")
	sb.WriteString("LIMIT 1;")

	return sb.String()
}

func QuerySupersetStatisRecordAgentAllASMPerAgent(date, domain, agents string) string {
	var sb strings.Builder

	tblName := fmt.Sprintf(opv.CST_TBL_ASMAgentAll, domain)

	sb.WriteString("SELECT")
	sb.WriteString(" SUM(srag.WinGold) AS WinGold,")
	sb.WriteString(" SUM(srag.LostGold) AS LostGold,")
	sb.WriteString(" SUM(srag.CellScore) AS CellScore,")
	sb.WriteString(" SUM(srag.Revenue) AS Revenue,")
	sb.WriteString(" SUM(srag.WinNum) AS WinNum,")
	sb.WriteString(" SUM(srag.LostNum) AS LostNum,")
	sb.WriteString(" SUM(srag.ActiveUsers) AS ActiveUsers ")
	sb.WriteString("FROM superset." + tblName + " srag ")
	sb.WriteString("WHERE srag.StatisDate = '" + date + "' ")
	sb.WriteString("AND srag.ChannelID IN (" + agents + ");")

	return sb.String()
}

func ReplaceAgentAll(date, rootAgent, tbl, sourceDB, sourceDBSub string, selResponse *swmysql.DBResponse) string {
	var sb strings.Builder

	sb.WriteString("REPLACE INTO `superset`.`" + tbl + "` (")
	sb.WriteString(" StatisDate,")
	sb.WriteString(" RootChannel,")
	sb.WriteString(" WinGold,")
	sb.WriteString(" LostGold,")
	sb.WriteString(" CellScore,")
	sb.WriteString(" Revenue,")
	sb.WriteString(" WinNum,")
	sb.WriteString(" LostNum,")
	sb.WriteString(" ActiveUsers,")
	sb.WriteString(" Subset,")
	sb.WriteString(" SourceDB,")
	sb.WriteString(" SourceDBSub")
	sb.WriteString(")")
	sb.WriteString("VALUES")

	for i := 0; i < int(selResponse.Length); i++ {

		tmpWinGold := selResponse.RowsResponse[i]["WinGold"]
		tmpLostGold := selResponse.RowsResponse[i]["LostGold"]
		tmpCellScore := selResponse.RowsResponse[i]["CellScore"]
		tmpRevenue := selResponse.RowsResponse[i]["Revenue"]
		tmpWinNum := selResponse.RowsResponse[i]["WinNum"]
		tmpLostNum := selResponse.RowsResponse[i]["LostNum"]
		tmpActiveUsers := selResponse.RowsResponse[i]["ActiveUsers"]

		syntax := fmt.Sprintf("('%v', %v, %v, %v, %v, %v, %v, %v, %v, '%v', '%v', '%v'),",
			date,
			opv.CHKNumVal(rootAgent),
			opv.CHKNumVal(tmpWinGold),
			opv.CHKNumVal(tmpLostGold),
			opv.CHKNumVal(tmpCellScore),
			opv.CHKNumVal(tmpRevenue),
			opv.CHKNumVal(tmpWinNum),
			opv.CHKNumVal(tmpLostNum),
			opv.CHKNumVal(tmpActiveUsers),
			"",
			sourceDB,
			sourceDBSub,
		)

		sb.WriteString(syntax)
	}

	return sb.String()[:len(sb.String())-1] + ";"
}
