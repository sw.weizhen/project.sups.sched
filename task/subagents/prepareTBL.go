package subagents

import (
	"errors"
	"fmt"

	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
	sq "gitlab.com/sw.weizhen/project.sups.sched/task/subagents/sqlscript"
	db "gitlab.com/sw.weizhen/rdbms.mysql"
)

func prepareTBL() error {
	dbdes := opv.DBDes
	dbdesPtr, err := db.New(dbdes.User, dbdes.Password, dbdes.Host, dbdes.Port, opv.CST_DB_Superset, dbdes.Charset, 1, 1)
	if err != nil {
		return err
	}
	defer dbdesPtr.Close()

	if err := createTBL(dbdesPtr); err != nil {
		return err
	}

	if err := createSubTBL(dbdesPtr); err != nil {
		return err
	}

	opv.L.Info("subagents::PrepareTBL done")

	return nil
}

func createTBL(dbdesPtr *db.DBOperator) error {

	if len(opv.DBSrcLs) == 0 {
		return errors.New("no found source db setting")
	}

	sqlSyntax := ""

	sqlSyntax += sq.CreateTBLAgentGame(opv.CST_TBL_SubagentBrandAgentGame) +
		sq.CreateTBLReguser(opv.CST_TBL_SubagentBrandAgentReguser) +
		sq.CreateTBLAgentAll(opv.CST_TBL_SubagentBrandAgentAll) +
		sq.CreateTBLStatisusers(opv.CST_TBL_SubagentBrandStatiusers)

	_, err := dbdesPtr.Exec(sqlSyntax)
	if err != nil {
		return fmt.Errorf("createTBL exec failed err -> %v", err)
	}

	return nil
}

func createSubTBL(dbdesPtr *db.DBOperator) error {
	if len(opv.DBSrcLs) == 0 {
		return errors.New("no found source db setting")
	}

	sqlSyntax := ""

	dbSrcLs := opv.DBSrcLs
	for _, dbSrc := range dbSrcLs {

		if !dbSrc.Enable {
			continue
		}

		domain := dbSrc.Domain
		dbname := dbSrc.Hostname

		tblAgentAll := fmt.Sprintf(opv.CST_TBL_SubagentDBNAgentAll, domain, dbname)
		tblAgentGame := fmt.Sprintf(opv.CST_TBL_SubagentDBNAgentGame, domain, dbname)
		tblReguser := fmt.Sprintf(opv.CST_TBL_SubagentDBNAgentReguser, domain, dbname)
		tblStatiusers := fmt.Sprintf(opv.CST_TBL_SubagentDBNStatisusers, domain, dbname)

		sqlSyntax += sq.CreateTBLAgentGame(tblAgentGame) +
			sq.CreateTBLReguser(tblReguser) +
			sq.CreateTBLAgentAll(tblAgentAll) +
			sq.CreateTBLStatisusers(tblStatiusers)

	}

	_, err := dbdesPtr.Exec(sqlSyntax)
	if err != nil {
		return fmt.Errorf("createSubTBL exec failed err -> %v", err)
	}

	return nil
}
