package subagents

import (
	"errors"
	"fmt"
	"strings"
	"time"

	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
	cf "gitlab.com/sw.weizhen/project.sups.sched/module/conf"
	sq "gitlab.com/sw.weizhen/project.sups.sched/task/subagents/sqlscript"
	db "gitlab.com/sw.weizhen/rdbms.mysql"
)

func computeAgentGMBrand(date, gm string, agentSet []string, dbsrcCfg cf.DBSrcCfg, dbdesptr *db.DBOperator) error {

	for _, agents := range agentSet {
		roots := strings.Split(agents, ",")
		if len(roots) <= 0 {
			continue
		}

		rootChl := roots[0]

		respData, err := dbdesptr.Query(sq.QuerySupersetStatisRecordAgentGameASMPerAgent(date, dbsrcCfg.Domain, gm, agents))
		if err != nil {
			return fmt.Errorf("QueryStatisRecordAgentGamePerAgent::%v", err)
		}

		if respData.Length <= 0 {
			return errors.New("QueryStatisRecordAgentGamePerAgent::no found data")
		}

		data := respData.RowsResponse[0]
		if len(data) <= 0 {
			continue
		}

		_, err = dbdesptr.Exec(sq.ReplaceAgentGame(date, gm, rootChl, opv.CST_TBL_SubagentBrandAgentGame, dbsrcCfg.Domain, dbsrcCfg.Hostname, respData))
		if err != nil {
			return fmt.Errorf("ReplaceAgentGame::%v", err)
		}

		subTBL := fmt.Sprintf(opv.CST_TBL_SubagentDBNAgentGame, dbsrcCfg.Domain, dbsrcCfg.Hostname)

		_, err = dbdesptr.Exec(sq.ReplaceAgentGame(date, gm, rootChl, subTBL, dbsrcCfg.Domain, dbsrcCfg.Hostname, respData))
		if err != nil {
			return fmt.Errorf("ReplaceAgentGame(Sub)::%v", err)
		}
	}

	return nil
}

func ComputeAgentGMBrandPerDay(date string, agentSet []string, dbsrcCfg cf.DBSrcCfg, dbdesptr *db.DBOperator) {
	gms, err := prepareAllGMs(date, dbsrcCfg.Domain, dbdesptr)
	if err != nil {
		opv.L.Error("%s(%s)::subagents::prepareAllGMs, err -> %v", dbsrcCfg.Domain, dbsrcCfg.Hostname, err)
		return
	}

	for _, gm := range gms {
		if err := computeAgentGMBrand(date, gm, agentSet, dbsrcCfg, dbdesptr); err != nil {
			opv.L.Error("%s(%s)::subagents::computeAgentGMBrand, err -> date: %s, gm: %s, %v", dbsrcCfg.Domain, dbsrcCfg.Hostname, date, gm, err)
			continue
		}
	}
}

func subagentsBrandGM(fromLastDate bool, head string, dbsrcCfg cf.DBSrcCfg, dbdesptr, dbsrcptr *db.DBOperator) error {

	days, err := prepareDays(fromLastDate, head, dbsrcCfg, dbdesptr)
	if err != nil {
		return fmt.Errorf("%s(%s)::subagents::subagentsBrandGM::prepareDays, err -> %v", dbsrcCfg.Domain, dbsrcCfg.Hostname, err)
	}

	roots, err := PrepareAgentRoots(dbsrcCfg, dbsrcptr)
	if err != nil {
		return fmt.Errorf("%s(%s)::subagents::subagentsBrandGM::prepareAgentRoots, err -> %v", dbsrcCfg.Domain, dbsrcCfg.Hostname, err)
	}

	agentSet := RootMapToSliceStr(roots)
	if len(agentSet) == 0 {
		return fmt.Errorf("%s(%s)::subagents::subagentsBrandGM::rootMapToSliceStr, err -> empty subagent container", dbsrcCfg.Domain, dbsrcCfg.Hostname)
	}

	// l.Info("%s(%s)asmAgents, agent: %s", dbsrcCfg.Domain, dbsrcCfg.Hostname, msgAgent(roots))

	t1 := time.Now().Unix()

	for _, date := range days {
		ComputeAgentGMBrandPerDay(date, agentSet, dbsrcCfg, dbdesptr)
	}

	cost := opv.FMTSecToHour(time.Now().Unix() - t1)

	opv.L.Info("subagents done => agent_game: %s(%s, %s) -> cost: %s", dbsrcCfg.Domain, dbsrcCfg.Hostname, days[0], cost)

	return nil
}
