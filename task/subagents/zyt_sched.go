package subagents

import (
	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
)

func Sched() {

	for i := 0; i < len(opv.CFG.RDBCfg.Source); i++ {

		dbsrcCfg := opv.CFG.RDBCfg.Source[i]

		cron := dbsrcCfg.Cron.Subagent
		head := dbsrcCfg.Head.Subagent

		if dbsrcCfg.Enable && len(cron) > 0 {

			opv.L.Info("add schedule(subagents) -> %s(%s) %s:%d, cron: %v", dbsrcCfg.Hostname, dbsrcCfg.Hostname, dbsrcCfg.Host, dbsrcCfg.Port, cron)

			opv.CRON.AddFunc(cron, func() {
				if err := recall(true, head, opv.CFG.CaptureCfg.Batch, dbsrcCfg); err != nil {
					opv.L.Error("schedule::subagents(%s, %s:%d), err -> %v", dbsrcCfg.Hostname, dbsrcCfg.Host, dbsrcCfg.Port, err)
				}
			})
		}
	}

}
