package subagents

import (
	"fmt"
	"strings"
	"time"

	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
	cf "gitlab.com/sw.weizhen/project.sups.sched/module/conf"
	sq "gitlab.com/sw.weizhen/project.sups.sched/task/subagents/sqlscript"
	db "gitlab.com/sw.weizhen/rdbms.mysql"
)

func ComputeStatisusersBrandPerDay(date string, agentSet []string, dbsrcCfg cf.DBSrcCfg, dbdesptr *db.DBOperator) {

	for _, agents := range agentSet {
		roots := strings.Split(agents, ",")
		if len(roots) <= 0 {
			continue
		}

		rootChl := roots[0]

		respData, err := dbdesptr.Query(sq.QuerySupersetStatisusersASMPerAgent(date, dbsrcCfg.Domain, agents))
		if err != nil {
			opv.L.Error("%s(%s)::QuerySupersetStatisusersASMPerAgent -> %v", dbsrcCfg.Domain, dbsrcCfg.Hostname, err)
			continue
		}

		if respData.Length <= 0 {
			opv.L.Error("%s(%s)::subagents::QuerySupersetStatisusersASMPerAgent -> %v no found data", date)
			continue
		}

		data := respData.RowsResponse[0]
		if len(data) <= 0 {
			continue
		}

		_, err = dbdesptr.Exec(sq.ReplaceStatisusers(date, rootChl, opv.CST_TBL_SubagentBrandStatiusers, dbsrcCfg.Domain, dbsrcCfg.Hostname, respData))
		if err != nil {
			opv.L.Error("%s(%s)::subagents::ReplaceStatisusers -> %v", dbsrcCfg.Domain, dbsrcCfg.Hostname, err)
			continue
		}

		subTBL := fmt.Sprintf(opv.CST_TBL_SubagentDBNStatisusers, dbsrcCfg.Domain, dbsrcCfg.Hostname)
		_, err = dbdesptr.Exec(sq.ReplaceStatisusers(date, rootChl, subTBL, dbsrcCfg.Domain, dbsrcCfg.Hostname, respData))
		if err != nil {
			opv.L.Error("%s(%s)::subagents::ReplaceStatisusers(Sub) -> %v", dbsrcCfg.Domain, dbsrcCfg.Hostname, err)
			continue
		}
	}

}

func subagentsBrandStatisusers(fromLastDate bool, head string, dbsrcCfg cf.DBSrcCfg, dbdesptr, dbsrcptr *db.DBOperator) error {

	days, err := prepareStatisusersDays(fromLastDate, head, dbsrcCfg, dbdesptr)
	if err != nil {
		return fmt.Errorf("%s(%s)::subagents::subagentsBrandStatisusers::prepareStatisusersDays -> %v", dbsrcCfg.Domain, dbsrcCfg.Hostname, err)
	}

	roots, err := PrepareAgentRoots(dbsrcCfg, dbsrcptr)
	if err != nil {
		return fmt.Errorf("%s(%s)::subagents::subagentsBrandStatisusers::prepareAgentRoots -> %v", dbsrcCfg.Domain, dbsrcCfg.Hostname, err)
	}

	agentSet := RootMapToSliceStr(roots)
	if len(agentSet) == 0 {
		return fmt.Errorf("%s(%s)::subagents::subagentsBrandStatisusers::rootMapToSliceStr -> empty subagent container", dbsrcCfg.Domain, dbsrcCfg.Hostname)
	}

	t1 := time.Now().Unix()

	for _, date := range days {
		ComputeStatisusersBrandPerDay(date, agentSet, dbsrcCfg, dbdesptr)
	}

	cost := opv.FMTSecToHour(time.Now().Unix() - t1)

	opv.L.Info("subagents done => statisusers: %s(%s, %s) -> cost: %s", dbsrcCfg.Domain, dbsrcCfg.Hostname, days[0], cost)

	return nil
}
