package verify

import (
	"fmt"
	"strings"

	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
	sq "gitlab.com/sw.weizhen/project.sups.sched/task/verify/sqlscript"
	db "gitlab.com/sw.weizhen/rdbms.mysql"
)

func selectPHRSumOnDay(yyyymm, statisDate, domain, dbname string, dbdesptr *db.DBOperator) (map[string]string, error) {

	dataResp, err := dbdesptr.Query(sq.VerifySumOnDayPHR(yyyymm, statisDate, dbname))
	if err != nil {
		return nil, fmt.Errorf("selectPHRSumOnDay::VerifySumOnDayPHR, err -> %v", err)
	}

	if dataResp.Length == 0 {
		return nil, fmt.Errorf("%s: %s unexpected error -> VerifySumOnDayPHR response length is 0", domain, dbname)
	}

	return dataResp.RowsResponse[0], nil
}

func verifyPHR(statisDate, domain, dbname string, dbdesptr *db.DBOperator) error {

	yyyymm := strings.ReplaceAll(statisDate, "-", "")[:6]

	phrData, err := selectPHRSumOnDay(yyyymm, statisDate, domain, dbname, dbdesptr)
	if err != nil {
		return err
	}

	tbl := fmt.Sprintf(opv.CST_TBL_PerHourAgentGame_yyyymm, yyyymm)
	cnt := phrData["cnt"]

	var description string = "phr data exception -> "
	var flag uint8 = 0

	if cnt == "0" {
		description += "no found data, "
		flag = flag | 1 //0001
	}

	zeroCnt := 0
	for _, phrV := range phrData {
		if phrV == "0" {
			zeroCnt++
		}
	}

	if zeroCnt >= 7 {
		description += "zero summary on day, "
		flag = flag | 2 //0010
	}

	if flag > 0 {
		if err := replaceExceptionDataLog(statisDate, description, tbl, domain, dbname, flag, dbdesptr); err != nil {
			return err
		}
	}

	if (flag & 1) > 0 {
		_, err := dbdesptr.Exec(sq.ReplaceFakePHR(yyyymm, statisDate, domain, dbname))
		if err != nil {
			return err
		}
	}

	return nil
}
