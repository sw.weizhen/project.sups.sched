package verify

import (
	"fmt"
	"strings"

	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
	sq "gitlab.com/sw.weizhen/project.sups.sched/task/verify/sqlscript"
	db "gitlab.com/sw.weizhen/rdbms.mysql"
)

func verifyASMAll(statisDate, domain, dbname string, dbdesptr *db.DBOperator) error {
	dataResp, err := dbdesptr.Query(sq.VerifySumOnDayGameAllASM(domain, dbname, statisDate))
	if err != nil {
		return fmt.Errorf("verifyASMAll::VerifySumOnDayGameAllASM -> error: %v", err)
	}

	if dataResp.Length == 0 {
		return fmt.Errorf("%s: %s unexpected error -> VerifySumOnDayGameAllASM response length is 0", domain, dbname)
	}

	yyyymm := strings.ReplaceAll(statisDate, "-", "")[:6]
	phrData, err := selectPHRSumOnDay(yyyymm, statisDate, domain, dbname, dbdesptr)
	if err != nil {
		return err
	}

	tbl := fmt.Sprintf(opv.CST_TBL_ASMAgentAll, domain)
	cnt := dataResp.RowsResponse[0]["cnt"]
	asmData := dataResp.RowsResponse[0]

	var description string = "data exception -> "
	var flag uint8 = 0

	if cnt == "0" {
		description += "no found data, "
		flag = flag | 1 //0001
	}

	zeroCnt := 0
	for _, asmV := range asmData {
		if asmV == "0" {
			zeroCnt++
		}
	}

	if zeroCnt >= 7 {
		description += "zero summary on day, "
		flag = flag | 2 //0010
	}

	notMatchList := ""
	for asmK, asmV := range asmData {
		phrV := phrData[asmK]

		if asmK == "ActiveUsers" || asmK == "cnt" {
			continue
		}

		if phrV != asmV {
			flag = flag | 4 //0100
			notMatchList += fmt.Sprintf("%s: %s(asm) -> %s(phr), ", asmK, asmV, phrV)
		}
	}

	if len(notMatchList) != 0 {
		notMatchList = "data does not match [" + notMatchList[:len(notMatchList)-2] + "]  "
		description += notMatchList
	}

	if flag > 0 {
		if err := replaceExceptionDataLog(statisDate, description, tbl, domain, dbname, flag, dbdesptr); err != nil {
			return err
		}
	}

	if (flag & 1) > 0 {
		_, err := dbdesptr.Exec(sq.ReplaceFakeAllASM(statisDate, domain, dbname))
		if err != nil {
			return err
		}
	}

	return nil
}
