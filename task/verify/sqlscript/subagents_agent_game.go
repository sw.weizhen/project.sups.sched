package sqlscript

import (
	"strings"

	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
)

func VerifySumOnDayGMSubsetBrand(domain, dbname, statisDate string) string {
	var sb strings.Builder

	sb.WriteString("SELECT")
	sb.WriteString(" COUNT(*) AS cnt,")
	sb.WriteString(" IFNULL(SUM(WinGold), 0) AS WinGold,")
	sb.WriteString(" IFNULL(SUM(LostGold), 0) AS LostGold,")
	sb.WriteString(" IFNULL(SUM(CellScore), 0) AS CellScore,")
	sb.WriteString(" IFNULL(SUM(Revenue), 0) AS Revenue,")
	sb.WriteString(" IFNULL(SUM(WinNum), 0) AS WinNum,")
	sb.WriteString(" IFNULL(SUM(LostNum), 0) AS LostNum,")
	sb.WriteString(" IFNULL(SUM(ActiveUsers), 0) AS ActiveUsers ")
	sb.WriteString("FROM superset." + opv.CST_TBL_SubagentBrandAgentGame + " ksraga ")
	sb.WriteString("WHERE ksraga.StatisDate = '" + statisDate + "' ")
	sb.WriteString("AND ksraga.SourceDB = '" + domain + "' ")
	sb.WriteString("AND ksraga.SourceDBSub = '" + dbname + "'; ")

	return sb.String()
}
