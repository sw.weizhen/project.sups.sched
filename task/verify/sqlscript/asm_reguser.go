package sqlscript

import (
	"fmt"
	"strings"

	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
)

func VerifySumOnDayReguserASM(domain, dbname, statisDate string) string {
	var sb strings.Builder

	tblName := fmt.Sprintf(opv.CST_TBL_ASMReguser, domain)

	sb.WriteString("SELECT")
	sb.WriteString(" COUNT(*) AS cnt,")
	sb.WriteString(" IFNULL(SUM(ksraara.ActiveUsers), 0) AS ActiveUsers ")
	sb.WriteString("FROM superset." + tblName + " ksraara ")
	sb.WriteString("WHERE ksraara.StatisDate = '" + statisDate + "' ")
	sb.WriteString("AND ksraara.SourceDBSub = '" + dbname + "'; ")
	// sb.WriteString("AND ksraara.ChannelID <> -1;")

	return sb.String()
}

func ReplaceFakeReguserASM(statisDate, domain, dbname string) string {
	var sb strings.Builder

	tblName := fmt.Sprintf(opv.CST_TBL_ASMReguser, domain)

	sb.WriteString("REPLACE INTO superset." + tblName)
	sb.WriteString("(StatisDate, ChannelID, WinGold, LostGold, CellScore, Revenue, WinNum, LostNum, ActiveUsers, SourceDB, SourceDBSub)")
	sb.WriteString("VALUES('" + statisDate + "', -1, 0, 0, 0, 0, 0, 0, 0, '" + domain + "', '" + dbname + "');")

	return sb.String()
}
