package sqlscript

import (
	"fmt"
	"strings"

	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
)

func ReplaceFakePHR(yyyymm, statisDate, domain, dbname string) string {
	var sb strings.Builder

	tblName := fmt.Sprintf(opv.CST_TBL_PerHourAgentGame_yyyymm, yyyymm)

	sb.WriteString("INSERT INTO superset." + tblName + " ")
	sb.WriteString("(StatisDateTime, ChannelID, GameID, WinGold, LostGold, CellScore, Revenue, WinNum, LostNum, ActiveUsers, FromSchema, FromTable, SourceDB, SourceDBSub)")
	sb.WriteString("VALUES('" + statisDate + "', -1, -1, 0, 0, 0, 0, 0, 0, 0, 'unknown', 'unknown', '" + domain + "', '" + dbname + "');")

	return sb.String()
}

func VerifySumOnDayPHR(yyyymm, statisDateTime, dbname string) string {
	var sb strings.Builder

	tblName := fmt.Sprintf(opv.CST_TBL_PerHourAgentGame_yyyymm, yyyymm)

	sb.WriteString("SELECT")
	sb.WriteString(" COUNT(*) AS cnt,")
	sb.WriteString(" IFNULL(SUM(WinGold), 0) as WinGold ,")
	sb.WriteString(" IFNULL(SUM(LostGold), 0) as LostGold ,")
	sb.WriteString(" IFNULL(SUM(CellScore), 0) as CellScore ,")
	sb.WriteString(" IFNULL(SUM(Revenue), 0) as Revenue ,")
	sb.WriteString(" IFNULL(SUM(WinNum), 0) as WinNum ,")
	sb.WriteString(" IFNULL(SUM(LostNum), 0) as LostNum ,")
	sb.WriteString(" IFNULL(SUM(ActiveUsers), 0) as ActiveUsers ")
	sb.WriteString("FROM superset." + tblName + " phr ")
	sb.WriteString("WHERE phr.StatisDateTime like '" + statisDateTime + "%'")
	sb.WriteString("AND phr.SourceDBSub = '" + dbname + "' ")

	return sb.String()
}
