package sqlscript

import (
	"fmt"
	"strings"

	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
)

func VerifySumOnDayStatisusersASM(domain, dbname, statisDate string) string {
	var sb strings.Builder

	tblName := fmt.Sprintf(opv.CST_TBL_ASMStatisusers, domain)

	sb.WriteString("SELECT")
	sb.WriteString(" COUNT(*) AS cnt,")
	sb.WriteString(" IFNULL(SUM(Y_RegUsers), 0) AS Y_RegUsers ")
	sb.WriteString("FROM superset." + tblName + " ksa ")
	sb.WriteString("WHERE ksa.StatisDate = '" + statisDate + "' ")
	sb.WriteString("AND ksa.SourceDBSub = '" + dbname + "'; ")
	// sb.WriteString("AND ksa.ChannelID <> -1;")

	return sb.String()
}

func ReplaceFakeStatisusersASM(statisDate, domain, dbname string) string {
	var sb strings.Builder

	tblName := fmt.Sprintf(opv.CST_TBL_ASMStatisusers, domain)

	sb.WriteString("REPLACE INTO superset." + tblName)
	sb.WriteString("(StatisDate, ChannelId, Y_NonLoginUsers, M_NonLoginUsers, H_NonLoginUsers, Y_RegUsers, M_RegUsers, H_RegUsers, Y_PayUsers, M_PayUsers, H_PayUsers, NextRegisterUser, NextLoginUser, ValidNextRegisterUser, ValidNextLoginUser, SevenRegisterUser, SevenLoginUser, ValidSevenRegisterUser, ValidSevenLoginUser, MonthRegisterUser, MonthLoginUser, ValidMonthRegisterUser, ValidMonthLoginUser, DayNewBetUsers, dayNewLogin, SourceDB, SourceDBSub)")
	sb.WriteString("VALUES('" + statisDate + "', -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '" + domain + "', '" + dbname + "');")

	return sb.String()
}
