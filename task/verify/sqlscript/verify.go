package sqlscript

import "strings"

func CreateTBLExceptionDataLog() string {
	var sb strings.Builder

	sb.WriteString("CREATE TABLE IF NOT EXISTS `superset`.`exception_data_log` (")
	sb.WriteString(" `StatisDate` date NOT NULL,")
	sb.WriteString(" `BitFlag` BIT(8) NOT NULL DEFAULT 0 COMMENT '0001:not found data, 0010:zero summary, 0100:data does not match',")
	sb.WriteString(" `Description` varchar(1000) NULL DEFAULT '',")
	sb.WriteString(" `SrcTable` varchar(50) NOT NULL DEFAULT '',")
	sb.WriteString(" `SourceDB` varchar(20) NOT NULL,")
	sb.WriteString(" `SourceDBSub` varchar(20) NOT NULL,")
	sb.WriteString(" `CreateDateTime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,")
	sb.WriteString(" PRIMARY KEY (`StatisDate`, `SrcTable`,`SourceDB`, `SourceDBSub`)")
	sb.WriteString(") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;")

	return sb.String()
}

func ReplaceExceptionDataLog(statisDate, bitFlag, description, srcTable, domain, dbname string) string {
	var sb strings.Builder

	sb.WriteString("REPLACE INTO superset.exception_data_log")
	sb.WriteString("(")
	sb.WriteString(" StatisDate, ")
	sb.WriteString(" BitFlag, ")
	sb.WriteString(" Description, ")
	sb.WriteString(" SrcTable, ")
	sb.WriteString(" SourceDB, ")
	sb.WriteString(" SourceDBSub ")
	sb.WriteString(")VALUES( ")
	sb.WriteString(" '" + statisDate + "', ")
	sb.WriteString(" " + bitFlag + ", ")
	sb.WriteString(" '" + description + "', ")
	sb.WriteString(" '" + srcTable + "', ")
	sb.WriteString(" '" + domain + "', ")
	sb.WriteString(" '" + dbname + "' ")
	sb.WriteString(");")

	return sb.String()
}
