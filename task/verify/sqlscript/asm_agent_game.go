package sqlscript

import (
	"fmt"
	"strings"

	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
)

func VerifySumOnDayGameASM(domain, dbname, statisDate string) string {
	var sb strings.Builder

	tblName := fmt.Sprintf(opv.CST_TBL_ASMAgentGame, domain)

	sb.WriteString("SELECT")
	sb.WriteString(" COUNT(*) AS cnt,")
	sb.WriteString(" IFNULL(SUM(WinGold), 0) AS WinGold,")
	sb.WriteString(" IFNULL(SUM(LostGold), 0) AS LostGold,")
	sb.WriteString(" IFNULL(SUM(CellScore), 0) AS CellScore,")
	sb.WriteString(" IFNULL(SUM(Revenue), 0) AS Revenue,")
	sb.WriteString(" IFNULL(SUM(WinNum), 0) AS WinNum,")
	sb.WriteString(" IFNULL(SUM(LostNum), 0) AS LostNum,")
	sb.WriteString(" IFNULL(SUM(ActiveUsers), 0) AS ActiveUsers ")
	sb.WriteString("FROM superset." + tblName + " ksraga ")
	sb.WriteString("WHERE ksraga.StatisDate = '" + statisDate + "' ")
	sb.WriteString("AND ksraga.SourceDBSub = '" + dbname + "'; ")
	// sb.WriteString("AND ksraga.ChannelID <> -1;")
	return sb.String()
}

func ReplaceFakeGameASM(statisDate, domain, dbname string) string {
	var sb strings.Builder

	tblName := fmt.Sprintf(opv.CST_TBL_ASMAgentGame, domain)

	sb.WriteString("INSERT INTO superset." + tblName)
	sb.WriteString("(StatisDate, ChannelID, GameID, WinGold, LostGold, CellScore, Revenue, WinNum, LostNum, ActiveUsers, SourceDB, SourceDBSub)")
	sb.WriteString("VALUES('" + statisDate + "', -1, -1, 0, 0, 0, 0, 0, 0, 0, '" + domain + "', '" + dbname + "');")

	return sb.String()
}
