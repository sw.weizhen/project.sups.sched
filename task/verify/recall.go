package verify

import (
	"time"

	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
	db "gitlab.com/sw.weizhen/rdbms.mysql"
	ct "gitlab.com/sw.weizhen/util.calc.timer"
)

func recall() error {

	dbdes := opv.DBDes

	opv.L.Info("VERIFY COMMENCE => %s, %s:%d", dbdes.Hostname, dbdes.Host, dbdes.Port)

	dbdesPtr, err := db.New(dbdes.User, dbdes.Password, dbdes.Host, dbdes.Port, opv.CST_DB_Superset, dbdes.Charset, 1, 1)
	if err != nil {
		return err
	}

	defer func() {
		dbdesPtr.Close()
		opv.L.Info("CEASE VERIFY => superset, %s:%d disconnected", dbdes.Host, dbdes.Port)
	}()

	dbsrcCfg := opv.DBSrcLs

	statisDate := ct.New().Add(-24 * 60 * 60).Layout(ct.CST_FMT_YMD)

	for _, db := range dbsrcCfg {

		if db.Enable {

			t1 := time.Now().Unix()

			if err := verifyASMReguser(statisDate, db.Domain, db.Hostname, dbdesPtr); err != nil {
				opv.L.Error("verifyASMReguser, err -> %v", err)
			}

			if err := verifyASMStatisusers(statisDate, db.Domain, db.Hostname, dbdesPtr); err != nil {
				opv.L.Error("verifyASMStatisusers, err -> %v", err)
			}

			if err := verifyPHR(statisDate, db.Domain, db.Hostname, dbdesPtr); err != nil {
				opv.L.Error("verifyPHR, err -> %v", err)
			}

			if err := verifyASMGame(statisDate, db.Domain, db.Hostname, dbdesPtr); err != nil {
				opv.L.Error("verifyASMGame, err -> %v", err)
			}

			if err := verifyASMAll(statisDate, db.Domain, db.Hostname, dbdesPtr); err != nil {
				opv.L.Error("verifyASMAll, err -> %v", err)
			}

			if err := verifyAgentSubset(statisDate, db.Domain, db.Hostname, dbdesPtr); err != nil {
				opv.L.Error("verifyASMAll, err -> %v", err)
			}

			cost := opv.FMTSecToHour(time.Now().Unix() - t1)

			opv.L.Info("%s(%s)::verify done => cost: %s", db.Domain, db.Hostname, cost)
		}
	}

	return nil
}
