package verify

import (
	"fmt"

	sq "gitlab.com/sw.weizhen/project.sups.sched/task/verify/sqlscript"
	db "gitlab.com/sw.weizhen/rdbms.mysql"
)

func replaceExceptionDataLog(statisDate, description, srcTable, domain, dbname string, bitFlag uint8, dbdesptr *db.DBOperator) error {

	description = description[:len(description)-2]
	description += "."

	syntax := sq.ReplaceExceptionDataLog(statisDate, fmt.Sprintf("%d", bitFlag), description, srcTable, domain, dbname)
	_, err := dbdesptr.Exec(syntax)

	return err
}
