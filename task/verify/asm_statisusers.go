package verify

import (
	"fmt"

	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
	sq "gitlab.com/sw.weizhen/project.sups.sched/task/verify/sqlscript"
	db "gitlab.com/sw.weizhen/rdbms.mysql"
)

func verifyASMStatisusers(statisDate, domain, dbname string, dbdesptr *db.DBOperator) error {

	dataResp, err := dbdesptr.Query(sq.VerifySumOnDayStatisusersASM(domain, dbname, statisDate))
	if err != nil {
		return fmt.Errorf("verifyASMStatisusers::VerifySumOnDayStatisusersASM, err -> %v", err)
	}

	if dataResp.Length == 0 {
		return fmt.Errorf("%s: %s unexpected error -> VerifySumOnDayStatisusersASM response length is 0", domain, dbname)
	}

	tbl := fmt.Sprintf(opv.CST_TBL_ASMStatisusers, domain)
	cnt := dataResp.RowsResponse[0]["cnt"]
	yRegUsers := dataResp.RowsResponse[0]["Y_RegUsers"]

	var description string = "field Y_RegUsers exception -> "
	var flag uint8 = 0

	if cnt == "0" {
		description += "no found data, "
		flag = flag | 1 //0001
	}

	if yRegUsers == "0" {
		description += "zero summary on day, "
		flag = flag | 2 //0010
	}

	if flag > 0 {
		if err := replaceExceptionDataLog(statisDate, description, tbl, domain, dbname, flag, dbdesptr); err != nil {
			return err
		}
	}

	if (flag & 1) > 0 {
		_, err := dbdesptr.Exec(sq.ReplaceFakeStatisusersASM(statisDate, domain, dbname))
		if err != nil {
			return err
		}
	}

	return nil
}
