package verify

import (
	"fmt"

	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
	sq "gitlab.com/sw.weizhen/project.sups.sched/task/verify/sqlscript"
	db "gitlab.com/sw.weizhen/rdbms.mysql"
)

func verifyASMReguser(statisDate, domain, dbname string, dbdesptr *db.DBOperator) error {
	dataResp, err := dbdesptr.Query(sq.VerifySumOnDayReguserASM(domain, dbname, statisDate))
	if err != nil {
		return fmt.Errorf("verifyASMReguser::VerifySumOnDayReguserASM, err -> %v", err)
	}

	if dataResp.Length == 0 {
		return fmt.Errorf("%s: %s unexpected error -> VerifySumOnDayReguserASM response length is 0", domain, dbname)
	}

	tbl := fmt.Sprintf(opv.CST_TBL_ASMReguser, domain)
	cnt := dataResp.RowsResponse[0]["cnt"]
	ActiveUsers := dataResp.RowsResponse[0]["ActiveUsers"]

	var description string = "field ActiveUsers exception -> "
	var flag uint8 = 0

	if cnt == "0" {
		description += "no found data, "
		flag = flag | 1 //0001
	}

	if ActiveUsers == "0" {
		description += "zero summary on day, "
		flag = flag | 2 //0010
	}

	if flag > 0 {
		if err := replaceExceptionDataLog(statisDate, description, tbl, domain, dbname, flag, dbdesptr); err != nil {
			return err
		}
	}

	if (flag & 1) > 0 {
		_, err := dbdesptr.Exec(sq.ReplaceFakeReguserASM(statisDate, domain, dbname))
		if err != nil {
			return err
		}
	}

	return nil
}
