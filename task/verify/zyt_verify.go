package verify

import (
	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
)

func Recall() {
	if err := prepareTBL(); err != nil {
		opv.L.Error("verify::prepareTBL failed, err -> %v", err)
		return
	}
}
