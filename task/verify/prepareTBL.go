package verify

import (
	"errors"
	"fmt"

	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
	sq "gitlab.com/sw.weizhen/project.sups.sched/task/verify/sqlscript"
	db "gitlab.com/sw.weizhen/rdbms.mysql"
)

func prepareTBL() error {
	dbdes := opv.DBDes
	dbdesPtr, err := db.New(dbdes.User, dbdes.Password, dbdes.Host, dbdes.Port, opv.CST_DB_Superset, dbdes.Charset, 1, 1)
	if err != nil {
		return err
	}
	defer dbdesPtr.Close()

	if err := createTBL(dbdesPtr); err != nil {
		return err
	}

	opv.L.Info("verify::PrepareTBL done")

	return nil
}

func createTBL(dbdesPtr *db.DBOperator) error {

	if len(opv.DBSrcLs) == 0 {
		return errors.New("no found source db setting")
	}

	sqlSyntax := ""

	sqlSyntax += sq.CreateTBLExceptionDataLog()

	_, err := dbdesPtr.Exec(sqlSyntax)
	if err != nil {
		return fmt.Errorf("createTBL exec failed err -> %v", err)
	}

	return nil
}
