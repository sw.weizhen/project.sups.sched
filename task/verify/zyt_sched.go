package verify

import (
	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
)

func Sched() {

	if len(opv.CFG.DataVerify.Cron) > 0 {

		opv.L.Info("add schedule(verify) -> %s(%s) %s:%d, cron: %v", opv.CFG.RDBCfg.Target[0].Hostname, opv.CFG.RDBCfg.Target[0].Hostname, opv.CFG.RDBCfg.Target[0].Host, opv.CFG.RDBCfg.Target[0].Port, opv.CFG.DataVerify.Cron)

		opv.CRON.AddFunc(opv.CFG.DataVerify.Cron, func() {
			recall()
		})
	}

}
