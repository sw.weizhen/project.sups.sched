package sqlscript

import (
	"fmt"
	"strings"

	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
	swmysql "gitlab.com/sw.weizhen/rdbms.mysql"
)

func CreateTBLGMsPerHour(yyyymm string) string {
	var sb strings.Builder

	tblName := fmt.Sprintf(opv.CST_TBL_PerHourAgentGame_yyyymm, yyyymm)

	sb.WriteString("CREATE TABLE IF NOT EXISTS `" + tblName + "` (")
	sb.WriteString(" `StatisDateTime` datetime NOT NULL,")
	sb.WriteString(" `ChannelID` int(11) NOT NULL,")
	sb.WriteString(" `GameID` int(11) NOT NULL,")
	sb.WriteString(" `WinGold` bigint(20) DEFAULT '0',")
	sb.WriteString(" `LostGold` bigint(20) DEFAULT '0',")
	sb.WriteString(" `CellScore` bigint(20) DEFAULT '0',")
	sb.WriteString(" `Revenue` bigint(20) DEFAULT '0',")
	sb.WriteString(" `WinNum` int(11) DEFAULT '0',")
	sb.WriteString(" `LostNum` int(11) DEFAULT '0',")
	sb.WriteString(" `ActiveUsers` int(11) DEFAULT '0',")
	sb.WriteString(" `FromSchema` varchar(50) NOT NULL DEFAULT '',")
	sb.WriteString(" `FromTable` varchar(50) NOT NULL DEFAULT '',")
	sb.WriteString(" `SourceDB` varchar(20) NOT NULL,")
	sb.WriteString(" `SourceDBSub` varchar(20) NOT NULL,")
	sb.WriteString(" `MoveDate` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,")
	sb.WriteString(" PRIMARY KEY (`StatisDateTime`, `ChannelID`, `GameID`, `SourceDBSub`)")
	sb.WriteString(") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;")

	return sb.String()
}

func QueryAllGMRecord(excludeSchema []string) string {
	var sb strings.Builder

	sb.WriteString("SELECT DISTINCT ist.TABLE_SCHEMA ")
	sb.WriteString("FROM information_schema.TABLES ist ")
	sb.WriteString("WHERE ist.TABLE_SCHEMA LIKE '%_record' ")
	sb.WriteString("AND ist.TABLE_SCHEMA <> 'game_record' ")
	sb.WriteString("AND ist.TABLE_SCHEMA <> 'userdiarylog_record' ")

	if len(excludeSchema) > 0 {
		sb.WriteString("AND ist.TABLE_SCHEMA NOT IN ( ")

		for _, schema := range excludeSchema {
			sb.WriteString("'" + schema + "',")
		}

		return sb.String()[:len(sb.String())-1] + ");"
	}

	return sb.String() + ";"
}

func QueryTBLsGameRecordPerMonth(tbSchema, month string) string {
	var sb strings.Builder

	sb.WriteString("SELECT isT.TABLE_NAME AS TBL ")
	sb.WriteString("FROM information_schema.TABLES isT ")
	sb.WriteString("WHERE TABLE_SCHEMA = '" + tbSchema + "' ")
	sb.WriteString("AND isT.TABLE_NAME LIKE 'gameRecord" + month + "%' ")
	sb.WriteString("ORDER BY isT.TABLE_NAME ASC;")

	return sb.String()
}

func QueryTBLsGameRecord(tbSchema, yyyymmdd string) string {
	var sb strings.Builder

	sb.WriteString("SELECT isT.TABLE_NAME AS TBL ")
	sb.WriteString("FROM information_schema.TABLES isT ")
	sb.WriteString("WHERE TABLE_SCHEMA = '" + tbSchema + "' ")
	sb.WriteString("AND isT.TABLE_NAME = 'gameRecord" + yyyymmdd + "' ")
	sb.WriteString("ORDER BY isT.TABLE_NAME ASC;")

	return sb.String()
}

func QueryCalcGMRecordPerHour(schema, table, startDateTime string, endDateTime ...string) string {
	var sb strings.Builder

	sb.WriteString("SELECT '" + startDateTime + "' AS StatisDate,")
	sb.WriteString(" ChannelID,")
	sb.WriteString(" gr.KindID AS GameID,")
	sb.WriteString(" SUM(AllBet) AS AllBet,")
	sb.WriteString(" SUM(CASE WHEN Profit > 0 THEN Profit ELSE 0 END) AS wingold,")
	sb.WriteString(" SUM(CASE WHEN Profit < 0 THEN Profit ELSE 0 END) AS lostgold,")
	sb.WriteString(" SUM(CellScore) AS CellScore,")
	sb.WriteString(" SUM(Revenue) AS Revenue,")
	sb.WriteString(" COUNT(CASE WHEN Profit >= 0 THEN Profit END) AS winNum,")
	sb.WriteString(" COUNT(CASE WHEN Profit < 0 THEN Profit END) AS lostNum,")
	sb.WriteString(" COUNT(DISTINCT gr.Accounts) AS ActiveUsers ")
	sb.WriteString("FROM " + schema + "." + table + " gr ")
	sb.WriteString("WHERE gr.GameEndTime >= '" + startDateTime + "'")

	if len(endDateTime) > 0 {
		sb.WriteString(" AND gr.GameEndTime < '" + endDateTime[0] + "' ")
	}

	sb.WriteString(" GROUP BY gr.ChannelID;")

	return sb.String()
}

func ReplacePerHourAgentGame(yyyymm, schema, tbl, srcDB, srcDBSub string, selResponse *swmysql.DBResponse) string {
	var sb strings.Builder

	tblName := fmt.Sprintf(opv.CST_TBL_PerHourAgentGame_yyyymm, yyyymm)

	sb.WriteString("REPLACE INTO superset." + tblName + " (")
	sb.WriteString(" StatisDateTime,")
	sb.WriteString(" ChannelID,")
	sb.WriteString(" GameID,")
	sb.WriteString(" WinGold,")
	sb.WriteString(" LostGold,")
	sb.WriteString(" CellScore,")
	sb.WriteString(" Revenue,")
	sb.WriteString(" WinNum,")
	sb.WriteString(" LostNum,")
	sb.WriteString(" ActiveUsers,")
	sb.WriteString(" FromSchema,")
	sb.WriteString(" FromTable,")
	sb.WriteString(" SourceDB,")
	sb.WriteString(" SourceDBSub")
	sb.WriteString(")")
	sb.WriteString("VALUES")

	for i := 0; i < int(selResponse.Length); i++ {

		tmpStatisDate := selResponse.RowsResponse[i]["StatisDate"]
		tmpChannelID := selResponse.RowsResponse[i]["ChannelID"]
		tmpGameID := selResponse.RowsResponse[i]["GameID"]
		// tmpAllBet := selResponse.RowsResponse[i]["AllBet"]
		tmpWingold := selResponse.RowsResponse[i]["wingold"]
		tmpLostgold := selResponse.RowsResponse[i]["lostgold"]
		tmpCellScore := selResponse.RowsResponse[i]["CellScore"]
		tmpRevenue := selResponse.RowsResponse[i]["Revenue"]
		tmpWinNum := selResponse.RowsResponse[i]["winNum"]
		tmpLostNum := selResponse.RowsResponse[i]["lostNum"]
		tmpActiveUsers := selResponse.RowsResponse[i]["ActiveUsers"]

		syntax := fmt.Sprintf("( '%s', %s, %s, %s, %s, %s, %s, %s, %s, %s, '%s', '%s', '%s', '%s'),",
			tmpStatisDate,
			opv.CHKNumVal(tmpChannelID),
			opv.CHKNumVal(tmpGameID),
			opv.CHKNumVal(tmpWingold),
			opv.CHKNumVal(tmpLostgold),
			opv.CHKNumVal(tmpCellScore),
			opv.CHKNumVal(tmpRevenue),
			opv.CHKNumVal(tmpWinNum),
			opv.CHKNumVal(tmpLostNum),
			opv.CHKNumVal(tmpActiveUsers),
			schema,
			tbl,
			srcDB,
			srcDBSub,
		)

		sb.WriteString(syntax)
	}

	return sb.String()[:len(sb.String())-1] + ";"
}

func QueryTBL(tblSchema, tblName string) string {
	var sb strings.Builder

	sb.WriteString("SELECT tbl.TABLE_NAME AS TABLE_NAME FROM information_schema.TABLES tbl ")
	sb.WriteString(fmt.Sprintf("WHERE tbl.TABLE_SCHEMA = '%s' ", tblSchema))
	sb.WriteString(fmt.Sprintf("AND tbl.TABLE_NAME = '%s';", tblName))

	return sb.String()
}
