package phr

import (
	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
	sq "gitlab.com/sw.weizhen/project.sups.sched/task/phr/sqlscript"
	db "gitlab.com/sw.weizhen/rdbms.mysql"
	ct "gitlab.com/sw.weizhen/util.calc.timer"
)

func prepareTBL() ([]string, error) {
	dbdes := opv.DBDes
	dbdesPtr, err := db.New(dbdes.User, dbdes.Password, dbdes.Host, dbdes.Port, opv.CST_DB_Superset, dbdes.Charset, 1, 1)
	if err != nil {
		return nil, err
	}
	defer dbdesPtr.Close()

	months, err := createTBL(dbdesPtr)
	if err != nil {
		return nil, err
	}

	opv.L.Info("phr::prepareTBL done")

	return months, nil
}

func prepareMonTBL(yyyymm string, dbdesPtr *db.DBOperator) error {
	_, err := dbdesPtr.Exec(sq.CreateTBLGMsPerHour(yyyymm))
	if err != nil {
		return err
	}

	return nil
}

func createTBL(dbdesPtr *db.DBOperator) ([]string, error) {

	months, err := opv.LsMonthTilToday(opv.CFG.Pvoid, ct.CST_FMT_YM_CMP)
	if err != nil {
		return make([]string, 0), err
	}

	tbls := ""
	for _, month := range months {
		tbls += sq.CreateTBLGMsPerHour(month)
	}

	_, err = dbdesPtr.Exec(tbls)
	if err != nil {
		return make([]string, 0), err
	}

	return months, nil
}
