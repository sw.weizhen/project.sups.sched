package phr

import (
	"fmt"
	"time"

	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
	cf "gitlab.com/sw.weizhen/project.sups.sched/module/conf"
	sq "gitlab.com/sw.weizhen/project.sups.sched/task/phr/sqlscript"
	db "gitlab.com/sw.weizhen/rdbms.mysql"
	ct "gitlab.com/sw.weizhen/util.calc.timer"
)

func phrAgentGame(month, schema, tbl string, dbSrcCfg cf.DBSrcCfg, dbdesPtr, dbsrcPtr *db.DBOperator) error {

	date := ct.New(
		ct.OptInput(tbl[10:]),
		ct.OptFMTinput(ct.CST_FMT_YMD_CMP),
	).Layout(ct.CST_FMT_YMD) + " 00:00:00"

	headT := ct.New(ct.OptInput(date))
	dayOnHour, err := headT.CALCTimeline(
		ct.OptLimitTime(ct.New(ct.OptInput(date)).Add(24*60*60)),
		ct.OptLimitTimeOverlap(false),
		ct.OptOutFMT(ct.CST_FMT_YMDHMS),
	)

	if err != nil {
		return fmt.Errorf("%s(%s)::phrAgentGame::CALCTimeline(%s) err -> %v", dbSrcCfg.Domain, dbSrcCfg.Hostname, date, err)
	}

	for i := 0; i < len(dayOnHour); i++ {
		head := dayOnHour[i]
		tail := ct.New(ct.OptInput(head)).Add(1 * 60 * 60).Layout(ct.CST_FMT_YMDHMS)

		syntax := sq.QueryCalcGMRecordPerHour(schema, tbl, head, tail)

		res, err := dbsrcPtr.Query(syntax)
		if err != nil {
			return fmt.Errorf("%s(%s)::phrAgentGame::QueryCalcGMRecordPerHour(%s, %s, %s, %s) err -> %v", dbSrcCfg.Domain, dbSrcCfg.Hostname, schema, tbl, head, tail, err)
		}

		if res.Length > 0 {
			_, err = dbdesPtr.Exec(sq.ReplacePerHourAgentGame(month, schema, tbl, dbSrcCfg.Domain, dbSrcCfg.Hostname, res))
			if err != nil {
				return fmt.Errorf("%s(%s)::phrAgentGame::ReplacePerHourAgentGame(%s, %s, %s, %s, %s) err -> %v", dbSrcCfg.Domain, dbSrcCfg.Hostname, month, schema, tbl, dbSrcCfg.Domain, dbSrcCfg.Hostname, err)
			}
		}
	}

	return nil
}

func phrAgentGameDays(month, gmRecord string, dbSrcCfg cf.DBSrcCfg, dbdesPtr, dbsrcPtr *db.DBOperator) error {

	gmRecordOnMonthRes, err := dbsrcPtr.Query(sq.QueryTBLsGameRecordPerMonth(gmRecord, month))
	if err != nil {
		return fmt.Errorf("phrAgentGameDays::QueryTBLsGameRecordPerMonth(%s, %s) err -> %v", gmRecord, month, err)
	}

	for i := 0; i < int(gmRecordOnMonthRes.Length); i++ {
		gmRecordyyyymmdd := gmRecordOnMonthRes.RowsResponse[i]["TBL"]
		date := ct.New(
			ct.OptInput(gmRecordyyyymmdd[10:]),
			ct.OptFMTinput(ct.CST_FMT_YMD_CMP),
		)

		baseDate := ct.New(
			ct.OptInput(dbSrcCfg.Head.PHR[:10]),
			ct.OptFMTinput(ct.CST_FMT_YMD),
		)

		if date.Compare(baseDate) >= 0 {
			if err := phrAgentGame(month, gmRecord, gmRecordyyyymmdd, dbSrcCfg, dbdesPtr, dbsrcPtr); err != nil {
				return err
			}
		}

	}

	return nil
}

func phrAgentGameMonth(month string, dbSrcCfg cf.DBSrcCfg, dbdesPtr, dbsrcPtr *db.DBOperator) error {
	qGMRs, err := dbsrcPtr.Query(sq.QueryAllGMRecord(dbSrcCfg.PerHourEXCLSchema))
	if err != nil {
		return fmt.Errorf("phrAgentGameMonth::QueryAllGMRecord(%s) err -> %v", month, err)
	}

	for _, gmsRecord := range qGMRs.RowsResponse {
		gmRecord := gmsRecord["TABLE_SCHEMA"]

		if err := phrAgentGameDays(month, gmRecord, dbSrcCfg, dbdesPtr, dbsrcPtr); err != nil {
			return err
		}
	}

	return nil
}

func phrAgentGameMonths(months []string, dbSrcCfg cf.DBSrcCfg, dbdesPtr, dbsrcPtr *db.DBOperator) error {

	t1 := time.Now().Unix()

	for _, month := range months {
		if err := phrAgentGameMonth(month, dbSrcCfg, dbdesPtr, dbsrcPtr); err != nil {
			return err
		}
	}

	cost := opv.FMTSecToHour(time.Now().Unix() - t1)

	opv.L.Info("phr done => %s(%s), cost: %s", dbSrcCfg.Domain, dbSrcCfg.Hostname, cost)

	return nil
}

func phrAgentGameYDAY(dbSrcCfg cf.DBSrcCfg, dbdesPtr, dbsrcPtr *db.DBOperator) error {

	t1 := time.Now().Unix()

	qGMRs, err := dbsrcPtr.Query(sq.QueryAllGMRecord(dbSrcCfg.PerHourEXCLSchema))
	if err != nil {
		return fmt.Errorf("phrAgentGameYDAY::QueryAllGMRecord(%s) err -> %v", dbSrcCfg.PerHourEXCLSchema, err)
	}

	yday_yyyymmdd := ct.New(
		ct.OptInput(ct.New().Add(-24*60*60).Layout(ct.CST_FMT_YMD)),
		ct.OptFMTinput(ct.CST_FMT_YMD),
	).Layout(ct.CST_FMT_YMD_CMP)

	yday_yyyymm := yday_yyyymmdd[:6]
	if err := prepareMonTBL(yday_yyyymm, dbdesPtr); err != nil {
		return fmt.Errorf("phrAgentGameYDAY::prepareMonTBL(%s) err -> %v", yday_yyyymm, err)
	}

	for _, gmsRecord := range qGMRs.RowsResponse {
		gmRecord := gmsRecord["TABLE_SCHEMA"]

		tbl := "gameRecord" + yday_yyyymmdd

		resTBL, err := dbsrcPtr.Query(sq.QueryTBL(gmRecord, tbl))
		if err != nil {
			opv.L.Error("%s(%s)::phrAgentGameYDAY::QueryTBL(%s, %s) err -> %v", dbSrcCfg.Domain, dbSrcCfg.Hostname, gmRecord, tbl, err)
			continue
		}

		if resTBL.Length < 1 {
			continue
		}

		qTBL := resTBL.RowsResponse[0]["TABLE_NAME"]

		if err := phrAgentGame(yday_yyyymm, gmRecord, qTBL, dbSrcCfg, dbdesPtr, dbsrcPtr); err != nil {
			return err
		}
	}

	cost := opv.FMTSecToHour(time.Now().Unix() - t1)

	opv.L.Info("phr done => %s(%s, %s) -> cost: %s", dbSrcCfg.Domain, dbSrcCfg.Hostname, yday_yyyymmdd, cost)

	return nil
}
