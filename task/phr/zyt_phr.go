package phr

import (
	"sync"

	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
	cf "gitlab.com/sw.weizhen/project.sups.sched/module/conf"
)

func Recall() {

	months, err := prepareTBL()
	if err != nil {
		opv.L.Error("phr::prepareTBL failed, err -> %v", err)
		return
	}

	var wg sync.WaitGroup

	for i := 0; i < len(opv.CFG.RDBCfg.Source); i++ {
		dbsrcCfg := opv.CFG.RDBCfg.Source[i]

		head := dbsrcCfg.Head.PHR

		if dbsrcCfg.Enable && len(head) > 0 {
			wg.Add(1)

			go func(dbSrcCfg cf.DBSrcCfg) {
				if err := recallMonths(head, months, dbsrcCfg); err != nil {
					opv.L.Error("PHR::RecallMonths(%s, %s:%d) err -> %v", dbsrcCfg.Hostname, dbsrcCfg.Host, dbsrcCfg.Port, err)
				}

				wg.Done()

			}(dbsrcCfg)
		}
	}

	wg.Wait()
	opv.L.Info("PHR DONE")

}
