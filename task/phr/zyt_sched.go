package phr

import (
	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
)

func Sched() {

	for i := 0; i < len(opv.CFG.RDBCfg.Source); i++ {

		srcdb := opv.CFG.RDBCfg.Source[i]

		cron := srcdb.Cron.PHR

		if srcdb.Enable && len(cron) > 0 {

			opv.L.Info("add schedule(phr) -> %s(%s) %s:%d, cron: %v", srcdb.Hostname, srcdb.Hostname, srcdb.Host, srcdb.Port, cron)

			opv.CRON.AddFunc(cron, func() {
				if err := recallYDAY(srcdb); err != nil {
					opv.L.Error("schedule::phr(%s, %s:%d), err -> %v", srcdb.Hostname, srcdb.Host, srcdb.Port, err)
				}
			})
		}

	}

}
