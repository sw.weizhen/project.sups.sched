package phr

import (
	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
	cf "gitlab.com/sw.weizhen/project.sups.sched/module/conf"
	db "gitlab.com/sw.weizhen/rdbms.mysql"

	syn "gitlab.com/sw.weizhen/project.sups.sched/sync.relay"
)

func recallMonths(head string, months []string, dbsrcCfg cf.DBSrcCfg) error {

	opv.L.Info("PHR RECALL COMMENCE => %s, %s:%d", dbsrcCfg.Hostname, dbsrcCfg.Host, dbsrcCfg.Port)

	dbdes := opv.DBDes
	dbdesPtr, err := db.New(dbdes.User, dbdes.Password, dbdes.Host, dbdes.Port, opv.CST_DB_Superset, dbdes.Charset, 1, 1)
	if err != nil {
		return err
	}

	dbsrcPtr, err := db.New(dbsrcCfg.User, dbsrcCfg.Password, dbsrcCfg.Host, dbsrcCfg.Port, opv.CST_DB_Default, dbsrcCfg.Charset, 1, 1)
	if err != nil {
		return err
	}

	defer func() {
		dbsrcPtr.Close()
		dbdesPtr.Close()

		opv.L.Info("PHR RECALL CEASE => %s, %s:%d disconnected", dbsrcCfg.Hostname, dbsrcCfg.Host, dbsrcCfg.Port)
	}()

	if err := phrAgentGameMonths(months, dbsrcCfg, dbdesPtr, dbsrcPtr); err != nil {
		return err
	}

	return nil
}

func recallYDAY(dbsrcCfg cf.DBSrcCfg) error {

	opv.L.Info("PHR RECALL COMMENCE => %s, %s:%d", dbsrcCfg.Hostname, dbsrcCfg.Host, dbsrcCfg.Port)

	metadata := syn.Commandeer(
		syn.CST_MD_TAG_PHR,
		dbsrcCfg.Domain,
		dbsrcCfg.Hostname,
		syn.CST_MD_TYPE_SCHEDULE,
		true,
		syn.CST_MD_PRIORITY_FORCE,
	)

	opv.L.Info("PHR RECALL PASS => %s, %s:%d", dbsrcCfg.Hostname, dbsrcCfg.Host, dbsrcCfg.Port)

	dbdes := opv.DBDes
	dbdesPtr, err := db.New(dbdes.User, dbdes.Password, dbdes.Host, dbdes.Port, opv.CST_DB_Superset, dbdes.Charset, 1, 1)
	if err != nil {
		return err
	}

	dbsrcPtr, err := db.New(dbsrcCfg.User, dbsrcCfg.Password, dbsrcCfg.Host, dbsrcCfg.Port, opv.CST_DB_Default, dbsrcCfg.Charset, 1, 1)
	if err != nil {
		return err
	}

	defer func() {
		dbsrcPtr.Close()
		dbdesPtr.Close()

		syn.CHLSchedDone <- *metadata

		opv.L.Info("PHR RECALL CEASE => %s, %s:%d disconnected", dbsrcCfg.Hostname, dbsrcCfg.Host, dbsrcCfg.Port)
	}()

	if err := phrAgentGameYDAY(dbsrcCfg, dbdesPtr, dbsrcPtr); err != nil {
		return err
	}

	return nil
}
