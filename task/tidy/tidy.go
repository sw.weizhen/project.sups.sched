package tidy

import (
	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
	db "gitlab.com/sw.weizhen/rdbms.mysql"
)

func tidy() error {

	opv.L.Info("TIDY COMMENCE => superset, %s:%d", opv.DBDes.Host, opv.DBDes.Port)

	dbdes := opv.DBDes
	dbdesPtr, err := db.New(dbdes.User, dbdes.Password, dbdes.Host, dbdes.Port, opv.CST_DB_Superset, dbdes.Charset, 1, 1)
	if err != nil {
		return err
	}

	defer func() {
		dbdesPtr.Close()
		opv.L.Info("TIDY CEASE => %s, %s:%d disconnected", opv.DBDes.Hostname, opv.DBDes.Host, opv.DBDes.Port)
	}()

	tidyData(dbdesPtr)

	return nil
}

func tidyData(dbdesptr *db.DBOperator) {
	dbs := opv.CFG.RDBCfg.Source
	for _, db := range dbs {
		delASMStatisusers(db.Domain, dbdesptr)
		delASMAgentAll(db.Domain, dbdesptr)
		delASMReguser(db.Domain, dbdesptr)
		delASMAgentGame(db.Domain, dbdesptr)
		delSubagentAgentAll(db.Domain, db.Hostname, dbdesptr)
		delSubagentAgentGame(db.Domain, db.Hostname, dbdesptr)
		delSubagentReguser(db.Domain, db.Hostname, dbdesptr)
		delSubagentStatisusers(db.Domain, db.Hostname, dbdesptr)
	}

	delSubagentsBrand(opv.CST_TBL_SubagentBrandAgentAll, dbdesptr)
	delSubagentsBrand(opv.CST_TBL_SubagentBrandAgentGame, dbdesptr)
	delSubagentsBrand(opv.CST_TBL_SubagentBrandStatiusers, dbdesptr)
	delSubagentsBrand(opv.CST_TBL_SubagentBrandAgentReguser, dbdesptr)

	delAgentGamePHR(dbdesptr)
}
