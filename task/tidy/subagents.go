package tidy

import (
	"fmt"

	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
	sq "gitlab.com/sw.weizhen/project.sups.sched/task/tidy/sqlscript"
	db "gitlab.com/sw.weizhen/rdbms.mysql"
	ct "gitlab.com/sw.weizhen/util.calc.timer"
)

func delSubagents(tbl string, days int, dbdesPtr *db.DBOperator) {

	resp, err := dbdesPtr.Query(sq.QueryTBL(opv.CST_DB_Superset, tbl))
	if err != nil {
		opv.L.Error("delSubagents::QueryTBL(%s) err -> %v", tbl, err)
		return
	}

	if resp.Length <= 0 {
		opv.L.Warning("delSubagents::QueryTBL(%s) err -> no found table", tbl)
		return
	}

	respTBL := resp.RowsResponse[0]["TABLE_NAME"]
	delDate := ct.New().Add(-days * 24 * 60 * 60).Layout(ct.CST_FMT_YMD)

	cnt, err := dbdesPtr.Exec(sq.DeleteDataWhereDate(opv.CST_DB_Superset, respTBL, delDate))
	if err != nil {
		opv.L.Error("delSubagents::DeleteDataWhereDate(%s) err -> %v", tbl, err)
		return
	}

	opv.L.Info("kill rows -> %d from %s(%s)", cnt, respTBL, delDate)
}

func delSubagentsBrand(tbl string, dbdesPtr *db.DBOperator) {

	days := opv.CFG.DataLifetime.Subagent

	delSubagents(tbl, days, dbdesPtr)
}

func delSubagentAgentAll(domain, dbname string, dbdesPtr *db.DBOperator) {

	tbl := fmt.Sprintf(opv.CST_TBL_SubagentDBNAgentAll, domain, dbname)
	days := opv.CFG.DataLifetime.Subagent

	delSubagents(tbl, days, dbdesPtr)
}

func delSubagentAgentGame(domain, dbname string, dbdesPtr *db.DBOperator) {

	tbl := fmt.Sprintf(opv.CST_TBL_SubagentDBNAgentGame, domain, dbname)
	days := opv.CFG.DataLifetime.Subagent

	delSubagents(tbl, days, dbdesPtr)
}

func delSubagentReguser(domain, dbname string, dbdesPtr *db.DBOperator) {

	tbl := fmt.Sprintf(opv.CST_TBL_SubagentDBNAgentReguser, domain, dbname)
	days := opv.CFG.DataLifetime.Subagent

	delSubagents(tbl, days, dbdesPtr)
}

func delSubagentStatisusers(domain, dbname string, dbdesPtr *db.DBOperator) {

	tbl := fmt.Sprintf(opv.CST_TBL_SubagentDBNStatisusers, domain, dbname)
	days := opv.CFG.DataLifetime.Subagent

	delSubagents(tbl, days, dbdesPtr)
}
