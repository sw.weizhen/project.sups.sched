package tidy

import (
	"strings"

	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
	sq "gitlab.com/sw.weizhen/project.sups.sched/task/tidy/sqlscript"
	db "gitlab.com/sw.weizhen/rdbms.mysql"
	ct "gitlab.com/sw.weizhen/util.calc.timer"
)

func delAgentGamePHR(dbdesPtr *db.DBOperator) {
	days := opv.CFG.DataLifetime.PHR
	delDate := ct.New().Add(-days * 24 * 60 * 60).Layout(ct.CST_FMT_YMD)
	baseDate := strings.ReplaceAll(delDate, "-", "")[:6]

	respKillTBL, err := dbdesPtr.Query(sq.QueryTBLToDrop(baseDate))
	if err != nil {
		opv.L.Error("delPHR::delAgentGamePHR(%s), QueryTBLToDrop error -> %v", respKillTBL, err)
		return
	}

	if respKillTBL.Length <= 0 {
		opv.L.Warning("delPHR::delAgentGamePHR(%s), baseDate: %v, no found tables", baseDate, err)
		return
	}

	tbls := make([]string, 0)
	for i := 0; i < int(respKillTBL.Length)-1; i++ {
		tbls = append(tbls, respKillTBL.RowsResponse[i]["TABLE_NAME"])
	}

	if len(tbls) >= 1 {
		dropSyntax := sq.DropTBLs(opv.CST_DB_Superset, tbls)
		_, err := dbdesPtr.Exec(dropSyntax)
		if err != nil {
			opv.L.Error("delPHR::delAgentGamePHR(%s), DropTBLs error -> %v", baseDate, err)
			return
		}

		opv.L.Info("delPHR::drop table: %v", tbls)
	}

	baseTBL := respKillTBL.RowsResponse[respKillTBL.Length-1]["TABLE_NAME"]
	cnt, err := dbdesPtr.Exec(sq.DeleteDataWhereDateTime(opv.CST_DB_Superset, baseTBL, delDate))
	if err != nil {
		opv.L.Error("delPHR::delAgentGamePHR(%s), DeleteDataWhereDateTime error -> %v", delDate, err)
		return
	}

	opv.L.Info("kill rows -> %d from %s(%s)", cnt, baseTBL, delDate)
}
