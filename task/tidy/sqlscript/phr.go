package sqlscript

import (
	"fmt"
	"strings"

	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
)

func QueryTBLToDrop(yyyymm string) string {
	var sb strings.Builder

	tblName := fmt.Sprintf(opv.CST_TBL_PerHourAgentGame_yyyymm, yyyymm)

	sb.WriteString("SELECT tbls.TABLE_NAME FROM ")
	sb.WriteString("( ")
	sb.WriteString(" SELECT tbl.TABLE_NAME AS TABLE_NAME FROM information_schema.TABLES tbl")
	sb.WriteString(" WHERE tbl.TABLE_SCHEMA = 'superset'")
	sb.WriteString(" AND tbl.TABLE_NAME like '" + opv.CST_TBL_PerHourAgentGame_ + "%'")
	sb.WriteString(" ) AS tbls ")
	sb.WriteString("WHERE tbls.TABLE_NAME <= '" + tblName + "';")

	return sb.String()
}
