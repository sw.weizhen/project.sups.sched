package sqlscript

import (
	"fmt"
	"strings"
)

func QueryTBL(tblSchema, tblName string) string {
	var sb strings.Builder

	sb.WriteString("SELECT tbl.TABLE_NAME AS TABLE_NAME FROM information_schema.TABLES tbl ")
	sb.WriteString(fmt.Sprintf("WHERE tbl.TABLE_SCHEMA = '%s' ", tblSchema))
	sb.WriteString(fmt.Sprintf("AND tbl.TABLE_NAME = '%s';", tblName))

	return sb.String()
}

func DeleteDataWhereDate(schema, tbl, date string) string {
	var sb strings.Builder

	sb.WriteString("DELETE FROM `" + schema + "`.`" + tbl + "` tbl ")
	sb.WriteString("where tbl.StatisDate < '" + date + "';")

	return sb.String()
}

func DeleteDataWhereDateTime(schema, tbl, date string) string {
	var sb strings.Builder

	sb.WriteString("DELETE FROM `" + schema + "`.`" + tbl + "` phr ")
	sb.WriteString("where phr.StatisDateTime < '" + date + "';")

	return sb.String()
}

func DropTBLs(schema string, tbls []string) string {
	var sb strings.Builder

	if len(tbls) == 0 {
		return "you such an idiot"
	}

	sb.WriteString("DROP TABLE ")
	for _, tbl := range tbls {
		sb.WriteString(schema + "." + tbl + ",")
	}

	return sb.String()[:len(sb.String())-1] + ";"
}

func QueryBrandLastDate(tbl, brand, sourceDB string) string {
	var sb strings.Builder

	sb.WriteString("SELECT sbs.StatisDate AS StatisDate ")
	sb.WriteString("FROM `superset`.`" + tbl + "` sbs ")
	sb.WriteString("WHERE sbs.Brand = '" + brand + "' ")
	sb.WriteString("AND sbs.SourceDBSub = '" + sourceDB + "' ")
	sb.WriteString("ORDER BY sbs.StatisDate DESC ")
	sb.WriteString("LIMIT 1;")

	return sb.String()
}
