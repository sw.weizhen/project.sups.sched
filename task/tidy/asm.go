package tidy

import (
	"fmt"

	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
	sq "gitlab.com/sw.weizhen/project.sups.sched/task/tidy/sqlscript"
	db "gitlab.com/sw.weizhen/rdbms.mysql"
	ct "gitlab.com/sw.weizhen/util.calc.timer"
)

func delASM(tbl string, days int, dbdesPtr *db.DBOperator) {

	resp, err := dbdesPtr.Query(sq.QueryTBL(opv.CST_DB_Superset, tbl))
	if err != nil {
		opv.L.Error("delASM::QueryTBL(%s) err -> %v", tbl, err)
		return
	}

	if resp.Length <= 0 {
		opv.L.Warning("delASM::QueryTBL(%s) err -> no found table", tbl)
		return
	}

	respTBL := resp.RowsResponse[0]["TABLE_NAME"]
	delDate := ct.New().Add(-days * 24 * 60 * 60).Layout(ct.CST_FMT_YMD)

	cnt, err := dbdesPtr.Exec(sq.DeleteDataWhereDate(opv.CST_DB_Superset, respTBL, delDate))
	if err != nil {
		opv.L.Error("delASM::DeleteDataWhereDate(%s) err -> %v", tbl, err)
		return
	}

	opv.L.Info("kill rows -> %d from %s(%s)", cnt, respTBL, delDate)
}

func delASMStatisusers(domain string, dbdesPtr *db.DBOperator) {

	tbl := fmt.Sprintf(opv.CST_TBL_ASMStatisusers, domain)
	days := opv.CFG.DataLifetime.ASMStatisusers

	delASM(tbl, days, dbdesPtr)
}

func delASMReguser(domain string, dbdesPtr *db.DBOperator) {

	tbl := fmt.Sprintf(opv.CST_TBL_ASMReguser, domain)
	days := opv.CFG.DataLifetime.ASMReguser

	delASM(tbl, days, dbdesPtr)
}

func delASMAgentGame(domain string, dbdesPtr *db.DBOperator) {

	tbl := fmt.Sprintf(opv.CST_TBL_ASMAgentGame, domain)
	days := opv.CFG.DataLifetime.ASMAgentGame

	delASM(tbl, days, dbdesPtr)
}

func delASMAgentAll(domain string, dbdesPtr *db.DBOperator) {

	tbl := fmt.Sprintf(opv.CST_TBL_ASMAgentAll, domain)
	days := opv.CFG.DataLifetime.ASMAgentAll

	delASM(tbl, days, dbdesPtr)
}
