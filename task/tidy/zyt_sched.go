package tidy

import (
	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
)

func Sched() {

	if len(opv.CFG.DataLifetime.Cron) > 0 {
		opv.L.Info("add schedule(tidy) -> %s(%s) %s:%d, cron: %v",
			opv.CFG.RDBCfg.Target[0].Hostname,
			opv.CFG.RDBCfg.Target[0].Hostname,
			opv.CFG.RDBCfg.Target[0].Host,
			opv.CFG.RDBCfg.Target[0].Port,
			opv.CFG.DataLifetime.Cron,
		)

		opv.CRON.AddFunc(opv.CFG.DataLifetime.Cron, func() {
			tidy()
		})
	}

}
