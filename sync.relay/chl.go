package syncrelay

var CHLSchedTrigger chan Metadata
var CHLSchedDone chan Metadata
var CHLSchedPass chan struct{}

var CHLMAMTrigger chan Metadata
var CHLMAMDeny chan struct{}
var CHLMAMDone chan Metadata
