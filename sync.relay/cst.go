package syncrelay

const (
	CST_MD_PRIORITY_LOW    = 50
	CST_MD_PRIORITY_MIDDLE = 25
	CST_MD_PRIORITY_HIGHT  = 1
	CST_MD_PRIORITY_FORCE  = 0
)

const (
	CST_MD_TYPE_SCHEDULE = "SCHED"
	CST_MD_TYPE_MANUAL   = "MAN"
)

const (
	CST_MD_TAG_ASM       = "ASM"
	CST_MD_TAG_PHR       = "PHR"
	CST_MD_TAG_SUM       = "SUM"
	CST_MD_TAG_SUBAGENTS = "SUBAGENTS"
)

const (
	CST_MD_TASK_AGENTGAME   = "agentgame"
	CST_MD_TASK_AGENTALL    = "agentall"
	CST_MD_TASK_REGUSER     = "reguser"
	CST_MD_TASK_STATISUSERS = "statisusers"
)
