package syncrelay

import (
	"sync"
)

type CMAPManual struct {
	mp map[string]Metadata
	sync.Mutex
}

func (ref *CMAPManual) Data() map[string]Metadata {
	ref.Lock()
	defer ref.Unlock()

	return ref.mp
}

func (ref *CMAPManual) Put(k string, metadata Metadata) {
	ref.Lock()
	defer ref.Unlock()

	ref.mp[k] = metadata
}

func (ref *CMAPManual) Get(k string) Metadata {
	ref.Lock()
	defer ref.Unlock()

	return ref.mp[k]
}

func (ref *CMAPManual) Len() int {

	return len(ref.mp)
}

func (ref *CMAPManual) Del(k string) {
	ref.Lock()
	defer ref.Unlock()

	delete(ref.mp, k)
}
