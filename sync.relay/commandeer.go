package syncrelay

import (
	"fmt"
	"time"
)

func Commandeer(tag, domain, db, typ string, forcePass bool, priority int) *Metadata {
	mpk := fmt.Sprintf("%s_%s_%s_%s", typ, tag, domain, db)
	timestamp := time.Now().Unix()

	metadata := &Metadata{
		Tag:       tag,
		Domain:    domain,
		DB:        db,
		Type:      typ,
		MPKey:     mpk,
		ForcePass: forcePass,
		Priority:  priority,
		Timestamp: timestamp,
	}

	MPSched.Put(mpk, *metadata)

	CHLSchedTrigger <- *metadata

	// block .............

	<-CHLSchedPass

	return metadata
}
