package syncrelay

import (
	"sync"
)

type CMAPSched struct {
	mp map[string]Metadata
	sync.Mutex
}

func (ref *CMAPSched) Data() map[string]Metadata {
	ref.Lock()
	defer ref.Unlock()

	return ref.mp
}

func (ref *CMAPSched) Put(k string, metadata Metadata) {
	ref.Lock()
	defer ref.Unlock()

	ref.mp[k] = metadata
}

func (ref *CMAPSched) Get(k string) Metadata {
	ref.Lock()
	defer ref.Unlock()

	return ref.mp[k]
}

func (ref *CMAPSched) Len() int {

	return len(ref.mp)
}

func (ref *CMAPSched) Del(k string) {
	ref.Lock()
	defer ref.Unlock()

	delete(ref.mp, k)
}
