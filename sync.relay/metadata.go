package syncrelay

type Metadata struct {
	Tag    string `json:"tag"`
	Domain string `json:"domain"`
	DB     string `json:"db"`
	Type   string `json:"type"`
	MPKey  string `json:"mpk"`

	ForcePass bool  `json:"pass"`
	Priority  int   `json:"priority"`
	Timestamp int64 `json:"timestamp"`

	ip string
}

func (ref *Metadata) IP() string {
	return ref.ip
}

func (ref *Metadata) SetIP(ip string) {
	ref.ip = ip
}
