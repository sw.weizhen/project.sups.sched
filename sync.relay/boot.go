package syncrelay

import (
	"time"

	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
)

func GoSyncRelay() {

	MPSched = CMAPSched{
		mp: make(map[string]Metadata),
	}

	MPManual = CMAPManual{
		mp: make(map[string]Metadata),
	}

	CHLSchedTrigger = make(chan Metadata, 1000)
	CHLSchedPass = make(chan struct{}, 1000)
	CHLSchedDone = make(chan Metadata, 1000)

	CHLMAMTrigger = make(chan Metadata, 1)
	CHLMAMDeny = make(chan struct{}, 1)
	CHLMAMDone = make(chan Metadata, 1)

	go syncRelay()
}

func syncRelay() {
	for {
		select {
		case mdMAM := <-CHLMAMTrigger:
			// reservice ...
			opv.L.Info("CHLMAMTrigger: %s", mdMAM.MPKey)

		case mdSched := <-CHLSchedTrigger:

			if MPManual.Len() > 0 {

				t1 := time.Now().Unix()

				CHLMAMDeny <- struct{}{}

				opv.L.Warning("waiting for manual operator done")

				metadata := <-CHLMAMDone

				opv.L.Info("%s done, cost: %v", metadata.MPKey, time.Now().Unix()-t1)

			}

			opv.L.Info("%s pass", mdSched.MPKey)

			// must pass ...
			CHLSchedPass <- struct{}{}

		case mdSched := <-CHLSchedDone:
			mpk := mdSched.MPKey
			MPSched.Del(mpk)
		}
	}
}
