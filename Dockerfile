FROM golang:1.18.1

RUN mkdir /gitlab.com/sw.weizhen/project.sups.sched

WORKDIR /gitlab.com/sw.weizhen/project.sups.sched

COPY go.mod .

RUN go mod tidy
COPY . .

RUN CGO_ENABLED=0
RUN GOOS=linux
RUN GOARCH=amd64
RUN go build -o ft_sched

EXPOSE 8089

ENTRYPOINT ["./ft_sched"]