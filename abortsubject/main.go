package main

import (
	"fmt"

	ct "gitlab.com/sw.weizhen/util.calc.timer"
)

func A() {
	head := "20221015"
	tail := "20221016"

	dayOnHour, err := ct.New(
		ct.OptInput(head),
		ct.OptFMTinput(ct.CST_FMT_YMD_CMP),
	).CALCTimeline(
		ct.OptLimitTime(ct.New(
			ct.OptInput(tail),
			ct.OptFMTinput(ct.CST_FMT_YMD_CMP),
		).Add(24*60*60)),
		ct.OptLimitTimeOverlap(true),
		ct.OptOutFMT(ct.CST_FMT_YMDHMS),
	)

	if err != nil {
		fmt.Println(err)
	}

	for _, v := range dayOnHour {
		fmt.Printf("%s\n", v)
	}
}

func B() {
	head := "20221015"
	tail := "20221016"

	days, err := ct.New(
		ct.OptInput(head),
		ct.OptFMTinput(ct.CST_FMT_YMD_CMP),
	).CALCTimeline(
		ct.OptLimitTime(ct.New(
			ct.OptInput(tail),
			ct.OptFMTinput(ct.CST_FMT_YMD_CMP),
		).Add(24*60*60)),
		ct.OptLimitTimeOverlap(false),
		ct.OptOutFMT(ct.CST_FMT_YMD),
		ct.OptINVLMode(ct.ENUM_INVL_DAY),
	)

	if err != nil {
		fmt.Println(err)
	}

	for _, day := range days {

		tl, err := ct.New(
			ct.OptInput(day),
			ct.OptFMTinput(ct.CST_FMT_YMD),
		).CALCTimeline(
			ct.OptLimitTime(ct.New(
				ct.OptInput(day),
				ct.OptFMTinput(ct.CST_FMT_YMD),
			).Add(24*60*60)),
			ct.OptLimitTimeOverlap(false),
			ct.OptOutFMT(ct.CST_FMT_YMDHMS),
		)

		if err != nil {
			fmt.Println(err)
			break
		}

		fmt.Println()
		fmt.Println(tl)
	}

}

func C() {

	ctHead := "20221001"
	ctTail := "20221001"

	tl, err := ct.New(
		ct.OptInput(ctHead),
		ct.OptFMTinput(ct.CST_FMT_YMD_CMP),
	).CALCTimeline(
		ct.OptLimitTime(
			ct.New(
				ct.OptInput(ctTail),
				ct.OptFMTinput(ct.CST_FMT_YMD_CMP),
			),
		),
		ct.OptINVLMode(ct.ENUM_INVL_DAY),
		ct.OptOutFMT(ct.CST_FMT_YMD),
	)

	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(tl)
}

func main() {
	fmt.Println("abortsubject::main()")

	C()

}
