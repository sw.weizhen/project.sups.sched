module gitlab.com/sw.weizhen/project.sups.sched

go 1.18

require (
	github.com/go-yaml/yaml v2.1.0+incompatible
	github.com/robfig/cron/v3 v3.0.1
	gitlab.com/sw.weizhen/rdbms.mysql v0.0.7
	gitlab.com/sw.weizhen/util.calc.timer v0.0.0
	gitlab.com/sw.weizhen/util.logger v0.0.0
)

require (
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
