package opvm

import (
	"log"

	"github.com/robfig/cron/v3"

	cf "gitlab.com/sw.weizhen/project.sups.sched/module/conf"
	core "gitlab.com/sw.weizhen/project.sups.sched/module/http.restful"
	lg "gitlab.com/sw.weizhen/util.logger"
)

func init() {
	log.Println("Booting server ...")

	L, ERR = lg.New(lg.SYS_WINDOWS, lg.MODE_DEBUG, 100, "./log/")
	if ERR != nil {
		log.Fatalf("server down. initializing logger error: %v", ERR)
	}

	CFG = cf.ConfigYML{}
	if err := cf.LoadServerConfig(CONFIG_PATH); err != nil {
		log.Fatalf("booting failed. reloading config error: %v", err)
	}

	CFG = cf.ConfYML

	if len(CFG.RDBCfg.Target) <= 0 {
		log.Fatal("booting failed. no found target db setting")
	}

	CRON = cron.New()
	if CRON == nil {
		log.Fatal("booting failed. nil CRON ptr")
	}

	DBDes = CFG.RDBCfg.Target[0]
	DBSrcLs = CFG.RDBCfg.Source
	if len(DBSrcLs) == 0 {
		L.Warning("no found source db setting")
	}

	AllowIP = make(map[string]bool, 0)
	if len(CFG.AllowIP) != 0 {
		for _, ip := range CFG.AllowIP {
			AllowIP[ip] = true
		}
	}

	MUX = core.SpreadRouter()

}
