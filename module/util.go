package opvm

import (
	"fmt"
	"strings"

	ct "gitlab.com/sw.weizhen/util.calc.timer"
)

func CHKNumVal(num string) string {

	val := num

	if len(val) > 0 {
		return val
	}

	return "0"
}

func FMTSecToHour(inSec int64) string {
	hours := inSec / 3600
	minutes := (inSec % 3600) / 60
	seconds := inSec % 60

	fmtHour := ""
	if hours > 0 {
		fmtHour += fmt.Sprintf("%dh:", hours)
	}

	if minutes > 0 {
		fmtHour += fmt.Sprintf("%dm:", minutes)
	}

	if seconds > 0 {
		fmtHour += fmt.Sprintf("%ds", seconds)
	}

	if len(fmtHour) == 0 {
		return "0s"
	}

	if strings.HasSuffix(fmtHour, ":") {
		return fmtHour[:len(fmtHour)-1]
	}

	return fmtHour
}

func LsMonthTilToday(head, fmtLayout string) ([]string, error) {
	monthUnit, err := ct.New(
		ct.OptInput(head),
		ct.OptFMTinput(ct.CST_FMT_YMD),
	).CALCTimeline(
		ct.OptINVLMode(ct.ENUM_INVL_MONTH),
		ct.OptLimitTimeOverlap(true),
		ct.OptOutFMT(fmtLayout),
	)

	if err != nil {
		return make([]string, 0), err
	}

	return monthUnit, nil
}
