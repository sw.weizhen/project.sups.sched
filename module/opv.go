package opvm

import (
	cf "gitlab.com/sw.weizhen/project.sups.sched/module/conf"
	core "gitlab.com/sw.weizhen/project.sups.sched/module/http.restful"
	lg "gitlab.com/sw.weizhen/util.logger"

	"github.com/robfig/cron/v3"
)

var L *lg.RotateLog
var CFG cf.ConfigYML
var ERR error

var DBDes cf.DBDesCfg
var DBSrcLs []cf.DBSrcCfg

var CRON *cron.Cron

var MUX *core.Multiplexer

var AllowIP map[string]bool
