package cfgreader

var ConfYML ConfigYML

type ConfigYML struct {
	RDBCfg       rdbCfg       `yaml:"rdb"`
	CaptureCfg   captureCfg   `yaml:"capture"`
	ServerCfg    serverCfg    `yaml:"server"`
	DataLifetime dataLifetime `yaml:"data-lifetime"`
	DataVerify   verifyCron   `yaml:"data-verify"`
	Pvoid        string       `yaml:"pvoid"`
	AllowIP      []string     `yaml:"allowip"`
}

type DBSrcCfg struct {
	Domain            string   `yaml:"domain"`
	Host              string   `yaml:"host"`
	Hostname          string   `yaml:"hostname"`
	Port              int      `yaml:"port"`
	User              string   `yaml:"user"`
	Password          string   `yaml:"password"`
	Charset           string   `yaml:"charset"`
	Enable            bool     `yaml:"enable"`
	Cron              dbCron   `yaml:"cron"`
	Head              head     `yaml:"head"`
	PerHourEXCLSchema []string `yaml:"perhourexclschema"`
}

type DBDesCfg struct {
	Host     string `yaml:"host"`
	Hostname string `yaml:"hostname"`
	Port     int    `yaml:"port"`
	User     string `yaml:"user"`
	Password string `yaml:"password"`
	Charset  string `yaml:"charset"`
}

type rdbCfg struct {
	Source []DBSrcCfg `yaml:"source"`
	Target []DBDesCfg `yaml:"target"`
}

type captureCfg struct {
	Batch int `yaml:"batch"`
}

type serverCfg struct {
	Port string `yaml:"port"`
}

type dataLifetime struct {
	PHR            int    `yaml:"phr"`
	ASMAgentGame   int    `yaml:"asm_agentgame"`
	ASMAgentAll    int    `yaml:"asm_agentall"`
	ASMReguser     int    `yaml:"asm_reguser"`
	ASMStatisusers int    `yaml:"asm_statisusers"`
	Subagent       int    `yaml:"subagent"`
	Cron           string `yaml:"cron"`
}

type verifyCron struct {
	Cron string `yaml:"cron"`
}

type head struct {
	ASM      string `yaml:"asm"`
	SUM      string `yaml:"sum"`
	PHR      string `yaml:"phr"`
	Subagent string `yaml:"subagent"`
}

type dbCron struct {
	ASM      string `yaml:"asm"`
	SUM      string `yaml:"sum"`
	PHR      string `yaml:"phr"`
	Subagent string `yaml:"subagent"`
}
