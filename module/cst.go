package opvm

const (
	CONFIG_PATH = "zftconfig.yml"
)

const (
	CST_DB_KYStatis = "KYStatis"
	CST_DB_Superset = "superset"
	CST_DB_Default  = ""
)

const (
	CST_TBL_SrcAgentGame   = "statis_record_agent_game"
	CST_TBL_SrcAgentAll    = "statis_record_agent_all"
	CST_TBL_SrcReguser     = "statis_record_agent_all_reguser"
	CST_TBL_SrcStatisusers = "statisusers"
)

const (
	CST_TBL_SubagentBrandAgentAll     = "subagent_brand_agentall"
	CST_TBL_SubagentBrandAgentGame    = "subagent_brand_agentgame"
	CST_TBL_SubagentBrandAgentReguser = "subagent_brand_reguser"
	CST_TBL_SubagentBrandStatiusers   = "subagent_brand_statisusers"
	CST_TBL_SubagentDBNAgentAll       = "subagent_%s_%s_agentall" // example: subagent_kxwc_kx-1_agentall <sourceDB, sourceDBSub>
	CST_TBL_SubagentDBNAgentGame      = "subagent_%s_%s_agentgame"
	CST_TBL_SubagentDBNAgentReguser   = "subagent_%s_%s_reguser"
	CST_TBL_SubagentDBNStatisusers    = "subagent_%s_%s_statisusers"
)

const (
	CST_TBL_SumBrandAgentAll    = "statis_record_agent_all_brand_summary"         //sum_brand_agentall
	CST_TBL_SumBrandAgentGame   = "statis_record_agent_game_brand_summary"        //sum_brand_agentall
	CST_TBL_SumBrandReguser     = "statis_record_agent_all_reguser_brand_summary" //sum_brand_reguser
	CST_TBL_SumBrandStatisusers = "statisusers_asm_brand_summary"                 //sum_brand_statisusers
)

const (
	CST_TBL_PerHourAgentGame_yyyymm = "statis_record_agent_game_per_hour_%s" //phr_agentgame_%s
	CST_TBL_PerHourAgentGame_       = "statis_record_agent_game_per_hour_"   //phr_agentgame_
)

const (
	CST_TBL_ASMAgentAll    = "%s_statis_record_agent_all_asm"         //asm_agentall
	CST_TBL_ASMAgentGame   = "%s_statis_record_agent_game_asm"        //asm_agentgame
	CST_TBL_ASMReguser     = "%s_statis_record_agent_all_reguser_asm" //asm_reguser
	CST_TBL_ASMStatisusers = "%s_statisusers_asm"                     //asm_statisusers
)
