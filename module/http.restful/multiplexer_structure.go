package corewebframe

import (
	"net/http"
	"sync"
)

var _ Router = &Multiplexer{}

//Multiplexer ...
type Multiplexer struct {
	inline bool

	radixTree *node
	pool      *sync.Pool

	handler                 http.Handler
	notFoundHandler         http.HandlerFunc
	methodNotAllowedHandler http.HandlerFunc
}

//InitMultiplexer ...
// func InitMultiplexer() *Multiplexer {
// 	Multiplexer := &Multiplexer{radixTree: &node{}, pool: &sync.Pool{}}
// 	Multiplexer.pool.New = func() interface{} {
// 		return newRouteContext()
// 	}
// 	return Multiplexer
// }

//InitRouter ...
// func InitRouter() *Multiplexer {
// 	return InitMultiplexer()
// }

// SpreadRouter ...
func SpreadRouter() *Multiplexer {
	Multiplexer := &Multiplexer{radixTree: &node{}, pool: &sync.Pool{}}
	Multiplexer.pool.New = func() interface{} {
		return newRouteContext()
	}
	return Multiplexer
}
