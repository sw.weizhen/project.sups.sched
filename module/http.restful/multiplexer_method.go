package corewebframe

import (
	"fmt"
	"net/http"
	"strings"
)

//ServeHTTP ...
func (mux *Multiplexer) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	if mux.handler == nil {
		mux.noFoundHandler().ServeHTTP(w, r)
		return
	}

	rctx, _ := r.Context().Value(RouteCtxKey).(*Context)
	if rctx != nil {
		mux.handler.ServeHTTP(w, r)
		return
	}

	rctx = mux.pool.Get().(*Context)
	rctx.CtxClear()

	rctx.Routes = mux
	rctx.parentCtx = r.Context()

	r = r.WithContext((*directContext)(rctx))

	mux.handler.ServeHTTP(w, r)
	mux.pool.Put(rctx)
}

//All ...
func (mux *Multiplexer) All(pattern string, handlerFn http.HandlerFunc) {
	mux.handle(methodALL, pattern, handlerFn)
}

//MethodFunc ...
func (mux *Multiplexer) MethodFunc(method string, pattern string, handlerFn http.HandlerFunc) {
	mux.funcMethod(method, pattern, handlerFn)
}

//Del ...
func (mux *Multiplexer) Del(pattern string, handlerFn http.HandlerFunc) {
	mux.handle(methodDEL, pattern, handlerFn)
}

//Get ...
func (mux *Multiplexer) Get(pattern string, handlerFn http.HandlerFunc) {
	mux.handle(methodGET, pattern, handlerFn)
}

//Post ...
func (mux *Multiplexer) Post(pattern string, handlerFn http.HandlerFunc) {
	mux.handle(methodPOST, pattern, handlerFn)
}

//Put ...
func (mux *Multiplexer) Put(pattern string, handlerFn http.HandlerFunc) {
	mux.handle(methodPUT, pattern, handlerFn)
}

//Options ...
func (mux *Multiplexer) Options(pattern string, handlerFn http.HandlerFunc) {
	mux.handle(methodOptions, pattern, handlerFn)
}

//RouteList ...
func (mux *Multiplexer) RouteList() []Route {
	return mux.radixTree.routes()
}

func (mux *Multiplexer) routeHTTP(w http.ResponseWriter, r *http.Request) {

	rctx := r.Context().Value(RouteCtxKey).(*Context)

	routePath := rctx.RoutePath
	if routePath == "" {
		if r.URL.RawPath != "" {
			routePath = r.URL.RawPath

		} else {
			routePath = r.URL.Path

		}
	}

	if rctx.RouteMethod == "" {
		rctx.RouteMethod = r.Method
	}

	method, ok := methodMap[rctx.RouteMethod]
	if !ok {
		mux.methNotAllowedHandler().ServeHTTP(w, r)

		return
	}

	if _, _, h := mux.radixTree.findRoute(rctx, method, routePath); h != nil {
		h.ServeHTTP(w, r)

		return
	}

	if rctx.methodNotAllowed {
		mux.methNotAllowedHandler().ServeHTTP(w, r)

	} else {
		mux.noFoundHandler().ServeHTTP(w, r)

	}
}

func (mux *Multiplexer) updateRouteHandler() {
	mux.handler = http.HandlerFunc(mux.routeHTTP)
}

func (mux *Multiplexer) noFoundHandler() http.HandlerFunc {
	if mux.notFoundHandler != nil {
		return mux.notFoundHandler
	}
	return http.NotFound
}

func (mux *Multiplexer) methNotAllowedHandler() http.HandlerFunc {
	if mux.methodNotAllowedHandler != nil {
		return mux.methodNotAllowedHandler
	}
	return methodNotAllowedHandler
}

func (mux *Multiplexer) handle(method methodType, pattern string, handler http.Handler) *node {
	if len(pattern) == 0 || pattern[0] != '/' {
		panic(fmt.Sprintf("route have to begin with '/' in '%s'", pattern))
	}

	if !mux.inline && mux.handler == nil {
		mux.updateRouteHandler()
	}

	var h http.Handler
	if mux.inline {
		mux.handler = http.HandlerFunc(mux.routeHTTP)

	} else {
		h = handler

	}

	return mux.radixTree.insertRoute(method, pattern, h)
}

func (mux *Multiplexer) funcMethod(method, pattern string, handler http.Handler) {
	m, ok := methodMap[strings.ToUpper(method)]
	if !ok {
		panic(fmt.Sprintf("http method: %s is not supported.", method))

	}

	mux.handle(m, pattern, handler)
}
