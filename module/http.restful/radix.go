package corewebframe

import (
	"fmt"
	"net/http"
	"regexp"
	"sort"
	"strings"
)

const (
	methodPOST methodType = 1 << iota
	methodGET
	methodPUT
	methodDEL
	methodOptions
)

const (
	ntStatic nodeType = iota
	ntRegexp
	ntParam
	ntCatchAll
)

//ItineraryFunc ...
type ItineraryFunc func(method string, route string, handler http.Handler) error

type methodType int
type nodeType uint8
type endpoints map[methodType]*endpoint

var methodALL = methodDEL | methodGET | methodPOST | methodPUT | methodOptions

var methodMap = map[string]methodType{
	http.MethodDelete:  methodDEL,
	http.MethodGet:     methodGET,
	http.MethodPost:    methodPOST,
	http.MethodPut:     methodPUT,
	http.MethodOptions: methodOptions,
}

//Route ...
type Route struct {
	Pattern  string
	Handlers map[string]http.Handler
}

type node struct {
	ndtype    nodeType
	lbl       byte
	tail      byte
	prefix    string
	rex       *regexp.Regexp
	endpoints endpoints
	children  [ntCatchAll + 1]nodes
}

type endpoint struct {
	handler   http.Handler
	pattern   string
	paramKeys []string
}

func (s endpoints) value(method methodType) *endpoint {
	mh, ok := s[method]
	if !ok {
		mh = &endpoint{}
		s[method] = mh
	}
	return mh
}

func (n *node) insertRoute(method methodType, pattern string, handler http.Handler) *node {
	var parent *node
	search := pattern

	for {
		if len(search) == 0 {
			n.setEndpoint(method, handler, pattern)

			return n
		}

		var lbl = search[0]
		var segTail byte
		var segEndIdx int
		var segTyp nodeType
		var segRexpat string

		if lbl == '{' || lbl == '*' {
			segTyp, _, segRexpat, segTail, _, segEndIdx = patNextSegment(search)

		}

		var prefix string
		if segTyp == ntRegexp {
			prefix = segRexpat

		}

		parent = n
		n = n.getEdge(segTyp, lbl, segTail, prefix)

		if n == nil {
			child := &node{lbl: lbl, tail: segTail, prefix: search}
			hn := parent.addChild(child, search)
			hn.setEndpoint(method, handler, pattern)

			return hn
		}

		if n.ndtype > ntStatic {
			search = search[segEndIdx:]

			continue
		}

		commonPrefix := longestPrefix(search, n.prefix)
		if commonPrefix == len(n.prefix) {

			search = search[commonPrefix:]
			continue
		}

		child := &node{
			ndtype: ntStatic,
			prefix: search[:commonPrefix],
		}
		parent.replaceChild(search[0], segTail, child)

		n.lbl = n.prefix[commonPrefix]
		n.prefix = n.prefix[commonPrefix:]
		child.addChild(n, n.prefix)

		search = search[commonPrefix:]
		if len(search) == 0 {
			child.setEndpoint(method, handler, pattern)

			return child
		}

		subchild := &node{
			ndtype: ntStatic,
			lbl:    search[0],
			prefix: search,
		}
		hn := child.addChild(subchild, search)
		hn.setEndpoint(method, handler, pattern)
		return hn
	}
}

func (n *node) addChild(child *node, prefix string) *node {
	search := prefix

	hn := child

	segTyp, _, segRexpat, segTail, segStartIdx, segEndIdx := patNextSegment(search)

	switch segTyp {
	case ntStatic:
	default:
		if segTyp == ntRegexp {
			rex, err := regexp.Compile(segRexpat)
			if err != nil {
				panic(fmt.Sprintf("invalid regexp pattern '%s' in route param", segRexpat))
			}

			child.prefix = segRexpat
			child.rex = rex
		}

		if segStartIdx == 0 {
			child.ndtype = segTyp

			if segTyp == ntCatchAll {
				segStartIdx = -1

			} else {
				segStartIdx = segEndIdx

			}

			if segStartIdx < 0 {
				segStartIdx = len(search)

			}

			child.tail = segTail

			if segStartIdx != len(search) {
				search = search[segStartIdx:]

				nn := &node{
					ndtype: ntStatic,
					lbl:    search[0],
					prefix: search,
				}

				hn = child.addChild(nn, search)
			}

		} else if segStartIdx > 0 {

			child.ndtype = ntStatic
			child.prefix = search[:segStartIdx]
			child.rex = nil

			search = search[segStartIdx:]

			nn := &node{
				ndtype: segTyp,
				lbl:    search[0],
				tail:   segTail,
			}

			hn = child.addChild(nn, search)
		}
	}

	n.children[child.ndtype] = append(n.children[child.ndtype], child)
	n.children[child.ndtype].Sort()

	return hn
}

func (n *node) replaceChild(lbl, tail byte, child *node) {
	for i := 0; i < len(n.children[child.ndtype]); i++ {
		if n.children[child.ndtype][i].lbl == lbl && n.children[child.ndtype][i].tail == tail {
			n.children[child.ndtype][i] = child
			n.children[child.ndtype][i].lbl = lbl
			n.children[child.ndtype][i].tail = tail

			return
		}
	}

	panic("missing child")
}

func (n *node) getEdge(ntyp nodeType, lbl, tail byte, prefix string) *node {
	nds := n.children[ntyp]
	for i := 0; i < len(nds); i++ {
		if nds[i].lbl == lbl && nds[i].tail == tail {
			if ntyp == ntRegexp && nds[i].prefix != prefix {

				continue
			}

			return nds[i]
		}
	}

	return nil
}

func (n *node) setEndpoint(method methodType, handler http.Handler, pattern string) {
	if n.endpoints == nil {
		n.endpoints = make(endpoints)
	}

	paramKeys := patParamKeys(pattern)

	if method&methodALL == methodALL {
		h := n.endpoints.value(methodALL)
		h.handler = handler
		h.pattern = pattern
		h.paramKeys = paramKeys
		for _, m := range methodMap {
			h := n.endpoints.value(m)
			h.handler = handler
			h.pattern = pattern
			h.paramKeys = paramKeys
		}
	} else {
		h := n.endpoints.value(method)
		h.handler = handler
		h.pattern = pattern
		h.paramKeys = paramKeys
	}
}

func (n *node) findRoute(rctx *Context, method methodType, path string) (*node, endpoints, http.Handler) {

	rctx.routePattern = ""
	rctx.routeParams.Keys = rctx.routeParams.Keys[:0]
	rctx.routeParams.Values = rctx.routeParams.Values[:0]

	rn := n.fdRoute(rctx, method, path)
	if rn == nil {
		return nil, nil, nil
	}

	rctx.URLParams.Keys = append(rctx.URLParams.Keys, rctx.routeParams.Keys...)
	rctx.URLParams.Values = append(rctx.URLParams.Values, rctx.routeParams.Values...)

	if rn.endpoints[method].pattern != "" {
		rctx.routePattern = rn.endpoints[method].pattern
		rctx.RoutePatterns = append(rctx.RoutePatterns, rctx.routePattern)
	}

	return rn, rn.endpoints, rn.endpoints[method].handler
}

func (n *node) fdRoute(rctx *Context, method methodType, path string) *node {
	nn := n
	search := path

	for t, nds := range nn.children {
		ntyp := nodeType(t)
		if len(nds) == 0 {
			continue
		}

		var xn *node
		xsearch := search

		var lbl byte
		if search != "" {
			lbl = search[0]
		}

		switch ntyp {
		case ntStatic:
			xn = nds.traceEdge(lbl)
			if xn == nil || !strings.HasPrefix(xsearch, xn.prefix) {
				continue
			}
			xsearch = xsearch[len(xn.prefix):]

		case ntParam, ntRegexp:

			if xsearch == "" {
				continue
			}

			for idx := 0; idx < len(nds); idx++ {
				xn = nds[idx]

				p := strings.IndexByte(xsearch, xn.tail)

				if p < 0 {
					if xn.tail == '/' {
						p = len(xsearch)
					} else {
						continue
					}

				} else if ntyp == ntRegexp && p == 0 {
					continue
				}

				if ntyp == ntRegexp && xn.rex != nil {
					if !xn.rex.MatchString(xsearch[:p]) {
						continue
					}
				} else if strings.IndexByte(xsearch[:p], '/') != -1 {

					continue
				}

				prevlen := len(rctx.routeParams.Values)
				rctx.routeParams.Values = append(rctx.routeParams.Values, xsearch[:p])
				xsearch = xsearch[p:]

				if len(xsearch) == 0 {
					if xn.isLeaf() {
						h := xn.endpoints[method]
						if h != nil && h.handler != nil {
							rctx.routeParams.Keys = append(rctx.routeParams.Keys, h.paramKeys...)
							return xn
						}

						rctx.methodNotAllowed = true
					}
				}

				fin := xn.fdRoute(rctx, method, xsearch)
				if fin != nil {
					return fin
				}

				rctx.routeParams.Values = rctx.routeParams.Values[:prevlen]
				xsearch = search
			}

			rctx.routeParams.Values = append(rctx.routeParams.Values, "")

		default:

			rctx.routeParams.Values = append(rctx.routeParams.Values, search)
			xn = nds[0]
			xsearch = ""
		}

		if xn == nil {
			continue
		}

		if len(xsearch) == 0 {
			if xn.isLeaf() {
				h := xn.endpoints[method]
				if h != nil && h.handler != nil {
					rctx.routeParams.Keys = append(rctx.routeParams.Keys, h.paramKeys...)
					return xn
				}

				rctx.methodNotAllowed = true
			}
		}

		fin := xn.fdRoute(rctx, method, xsearch)
		if fin != nil {
			return fin
		}

		if xn.ndtype > ntStatic {
			if len(rctx.routeParams.Values) > 0 {
				rctx.routeParams.Values = rctx.routeParams.Values[:len(rctx.routeParams.Values)-1]
			}
		}

	}

	return nil
}

func (n *node) isLeaf() bool {
	return n.endpoints != nil
}

func (n *node) routes() []Route {
	rts := []Route{}

	n.footprint(func(eps endpoints) bool {

		pats := make(map[string]endpoints)

		for mt, h := range eps {
			if h.pattern == "" {
				continue
			}
			p, ok := pats[h.pattern]
			if !ok {
				p = endpoints{}
				pats[h.pattern] = p
			}
			p[mt] = h
		}

		for p, mh := range pats {
			hs := make(map[string]http.Handler)
			if mh[methodALL] != nil && mh[methodALL].handler != nil {
				hs["*"] = mh[methodALL].handler
			}

			for mt, h := range mh {
				if h.handler == nil {
					continue
				}
				m := methodTypeString(mt)
				if m == "" {
					continue
				}
				hs[m] = h.handler
			}

			rt := Route{p, hs}
			rts = append(rts, rt)
		}

		return false
	})

	return rts
}

func (n *node) footprint(fn func(eps endpoints) bool) bool {

	if (n.endpoints != nil) && fn(n.endpoints) {
		return true
	}

	for _, ns := range n.children {
		for _, cn := range ns {
			if cn.footprint(fn) {
				return true
			}
		}
	}
	return false
}

func patNextSegment(pattern string) (nodeType, string, string, byte, int, int) {
	ps := strings.Index(pattern, "{")
	ws := strings.Index(pattern, "*")

	if ps < 0 && ws < 0 {
		return ntStatic, "", "", 0, 0, len(pattern)
	}

	if ps >= 0 && ws >= 0 && ws < ps {
		panic("wildcard '*' must be the last pattern'")
	}

	var tail byte = '/'

	if ps >= 0 {

		nt := ntParam

		cCnt := 0
		pe := ps
		for i, c := range pattern[ps:] {
			if c == '{' {
				cCnt++
			} else if c == '}' {
				cCnt--
				if cCnt == 0 {
					pe = ps + i
					break
				}
			}
		}
		if pe == ps {
			panic("route param closing delimiter '}' is missing")
		}

		key := pattern[ps+1 : pe]
		pe++

		if pe < len(pattern) {
			tail = pattern[pe]
		}

		var rexpat string
		if idx := strings.Index(key, ":"); idx >= 0 {
			nt = ntRegexp
			rexpat = key[idx+1:]
			key = key[:idx]
		}

		if len(rexpat) > 0 {
			if rexpat[0] != '^' {
				rexpat = "^" + rexpat
			}
			if rexpat[len(rexpat)-1] != '$' {
				rexpat += "$"
			}
		}

		return nt, key, rexpat, tail, ps, pe
	}

	if ws < len(pattern)-1 {
		panic("wildcard '*' must be the last value in a route. trim trailing text or use a '{param}' instead")
	}
	return ntCatchAll, "*", "", 0, ws, len(pattern)
}

func patParamKeys(pattern string) []string {
	pat := pattern
	paramKeys := []string{}
	for {
		ptyp, paramKey, _, _, _, e := patNextSegment(pat)
		if ptyp == ntStatic {
			return paramKeys
		}
		for i := 0; i < len(paramKeys); i++ {
			if paramKeys[i] == paramKey {
				panic(fmt.Sprintf("routing pattern '%s', contains duplicate parameter key, '%s'", pattern, paramKey))
			}
		}
		paramKeys = append(paramKeys, paramKey)
		pat = pat[e:]
	}
}

func longestPrefix(k1, k2 string) int {
	max := len(k1)
	if l := len(k2); l < max {
		max = l
	}
	var i int
	for i = 0; i < max; i++ {
		if k1[i] != k2[i] {
			break
		}
	}
	return i
}

func methodTypeString(method methodType) string {
	for s, t := range methodMap {
		if method == t {
			return s
		}
	}
	return ""
}

type nodes []*node

func (ns nodes) Sort() {
	sort.Sort(ns)
	ns.tailSort()
}

func (ns nodes) Len() int {
	return len(ns)
}

func (ns nodes) Swap(i, j int) {
	ns[i], ns[j] = ns[j], ns[i]
}

func (ns nodes) Less(i, j int) bool {
	return ns[i].lbl < ns[j].lbl
}

func (ns nodes) tailSort() {
	for i := len(ns) - 1; i >= 0; i-- {
		if ns[i].ndtype > ntStatic && ns[i].tail == '/' {
			ns.Swap(i, len(ns)-1)
			return
		}
	}
}

func (ns nodes) traceEdge(lbl byte) *node {
	num := len(ns)
	idx := 0
	i, j := 0, num-1

	for i <= j {
		idx = i + (j-i)/2

		if lbl > ns[idx].lbl {
			i = idx + 1

		} else if lbl < ns[idx].lbl {
			j = idx - 1

		} else {
			i = num

		}
	}

	if ns[idx].lbl != lbl {
		return nil

	}

	return ns[idx]
}
