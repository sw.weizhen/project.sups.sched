package httpresp

import (
	"bytes"
	"encoding/json"
)

type RespDataABS struct {
	Success      bool        `json:"success"`
	ErrCode      int         `json:"errCode"`
	ErrMsg       string      `json:"errMsg"`
	ResponseData interface{} `json:"data"`
}

func CodeResponseAbstraction(success bool, errCode int, errMsg string, rtnData interface{}, setEscpHTML bool) []byte {
	var respData RespDataABS = RespDataABS{
		ErrCode:      errCode,
		ErrMsg:       errMsg,
		Success:      success,
		ResponseData: rtnData,
	}

	byteBuf := bytes.NewBuffer([]byte{})
	encoder := json.NewEncoder(byteBuf)
	encoder.SetEscapeHTML(setEscpHTML)
	encoder.Encode(respData)

	return byteBuf.Bytes()
}

func CodeResponseSuccess(respData interface{}) []byte {
	return CodeResponseAbstraction(true, -1, "", respData, true)
}
