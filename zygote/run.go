package zygote

import (
	"fmt"
	"log"
	"net/http"

	opv "gitlab.com/sw.weizhen/project.sups.sched/module"
	syn "gitlab.com/sw.weizhen/project.sups.sched/sync.relay"

	asm "gitlab.com/sw.weizhen/project.sups.sched/task/asm"
	phr "gitlab.com/sw.weizhen/project.sups.sched/task/phr"
	subagents "gitlab.com/sw.weizhen/project.sups.sched/task/subagents"
	sum "gitlab.com/sw.weizhen/project.sups.sched/task/sum"

	tidy "gitlab.com/sw.weizhen/project.sups.sched/task/tidy"
	verify "gitlab.com/sw.weizhen/project.sups.sched/task/verify"
	wm "gitlab.com/sw.weizhen/project.sups.sched/task/webmanual"
)

func Run() {

	syn.GoSyncRelay()

	asm.Recall()
	sum.Recall()
	phr.Recall()
	subagents.Recall()
	verify.Recall()

	phr.Sched()
	asm.Sched()
	sum.Sched()
	subagents.Sched()

	tidy.Sched()
	verify.Sched()

	opv.CRON.Start()

	opv.L.Info("SCHEDULE ACTIVATED")

	wm.Reg()

	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", opv.CFG.ServerCfg.Port), opv.MUX))

}
